Attribute VB_Name = "mdlBXLLIB"

'------------------------------------------------------------------------------------------------------
'  Constant Variables
'------------------------------------------------------------------------------------------------------

'   Rotation
Public Const ROTATE_0 As Long = 0
Public Const ROTATE_90 As Long = 1
Public Const ROTATE_180 As Long = 2
Public Const ROTATE_270 As Long = 3

'   Bar-code Type
Public Const CODE39 As Long = 0
Public Const CODE128 As Long = 1
Public Const I2OF5 As Long = 2
Public Const CODEBAR As Long = 3
Public Const CODE93 As Long = 4
Public Const UPC_A As Long = 5
Public Const UPC_E As Long = 6
Public Const EAN13 As Long = 7
Public Const EAN8 As Long = 8
Public Const UCC_EAN128 As Long = 9

'   Device Fonts
Public Const ENG_9X15 As Long = 0   '    9 x 15
Public Const ENG_12X20 As Long = 1  '    12 x 20
Public Const ENG_16X25 As Long = 2  '    16 x 25
Public Const ENG_19X30 As Long = 3  '    19 x 30
Public Const ENG_24X38 As Long = 4  '    24 x 38
Public Const ENG_32X50 As Long = 5  '    32 x 50
Public Const ENG_48X76 As Long = 6  '    48 x 76
Public Const ENG_22X34 As Long = 7    '  22 x 34
Public Const ENG_28X44 As Long = 8    '  28 x 44
Public Const ENG_37X58 As Long = 9    '  37 x 58

Public Const KOR_16X16 As Long = 97  '0x61   '   16 x 16
Public Const KOR_24X24 As Long = 98  '0x62   '   24 x 24
Public Const KOR_20X20 As Long = 99  '0x63 '  20 x 20
Public Const KOR_26X26 As Long = 100    '0x64 '  26 x 26
Public Const KOR_20X26 As Long = 101    '0x65 '  20 x 26
Public Const KOR_38X38 As Long = 102    '0x66 '  38 x 38

Public Const CHN_GB2312 As Long = 96 '0x6D
Public Const CHN_BIG5 As Long = 110  '0x6E

'   Speed
Public Const SPEED_25 As Long = 0
Public Const SPEED_30 As Long = 1
Public Const SPEED_40 As Long = 2
Public Const SPEED_50 As Long = 3
Public Const SPEED_60 As Long = 4
Public Const SPEED_70 As Long = 5

'   Orientation
Public Const TOP As Long = 0
Public Const BOTTOM As Long = 1

'   Media Type
Public Const GAP As Long = 0
Public Const CONTINUOUS As Long = 1
Public Const BLACKMARK As Long = 2

'   Block Option
Public Const LINE_OVER_WRITING As Long = 0
Public Const LINE_EXCLUSIVE_OR As Long = 1
Public Const LINE_DELETE As Long = 2
Public Const SLOPE As Long = 3
Public Const BOX As Long = 4

' Font Selection
Public Const ASCII As String = "U"
Public Const KS5601 As String = "K"
Public Const BIG5 As String = "B"
Public Const GB2312 As String = "G"
Public Const ShiftJIS As String = "J"


' Font Alignment
Public Const LEFTALIGN As String = "L"
Public Const RIGHTALIGN As String = "R"
Public Const CENTERALIGN As String = "C"

' Font Direction
Public Const LEFTTORIGHT As Long = 0
Public Const RIGHTTOLEFT As Long = 1

' QRCode MODEL
Public Const QRMODEL_1 As Long = 1
Public Const QRMODEL_2 As Long = 2

' QRCode ECC Level
Public Const QRECCLEVEL_L As Long = 1   ' 7%
Public Const QRECCLEVEL_M As Long = 2   ' 15%
Public Const QRECCLEVEL_Q As Long = 3   ' 25%
Public Const QRECCLEVEL_H As Long = 4   ' 30%

' QRCode size
Public Const QRSIZE_1 As Long = 1
Public Const QRSIZE_2 As Long = 2
Public Const QRSIZE_3 As Long = 3
Public Const QRSIZE_4 As Long = 4

' Dither option
Public Const DITHER_NONE As Long = -1
Public Const DITHER_1 As Long = 0
Public Const DITHER_2 As Long = 1
Public Const DITHER_3 As Long = 6
Public Const DITHER_4 As Long = 7

'   Alignment
Public Const ALIGN_LEFT = 0
Public Const ALIGN_CENTER = 1
Public Const ALIGN_RIGHT = 2
Public Const ALIGN_BOTH_SIDE = 3


'------------------------------------------------------------------------------------------------------
' DLL API Function
'------------------------------------------------------------------------------------------------------

Public Declare Function ConnectPrinter Lib "BXLLIB.dll" (ByVal szPrinterName As String) As Boolean

Public Declare Function DisconnectPrinter Lib "BXLLIB.dll" () As Boolean


Public Declare Function GetBIXOLON_PrinterList Lib "BXLLIB.dll" (ByVal strInstalledPrinter As Any) As Long

Public Declare Function GetDllVersion Lib "BXLLIB.dll" (ByVal strBxlPrtList As Any) As Boolean


Public Declare Function GetPrinterResolution Lib "BXLLIB.dll" () As Long


Public Declare Function Print1DBarcode Lib "BXLLIB.dll" (ByVal nHorizontalPos As Long, _
                                             ByVal nVerticalPos As Long, _
                                             ByVal nBarcodeType As Long, _
                                             ByVal nNarrowBarWidth As Long, _
                                             ByVal nWideBarWidth As Long, _
                                             ByVal nBarcodeHeight As Long, _
                                             ByVal nRotation As Long, _
                                             ByVal bHRI As Boolean, _
                                             ByVal pData As String) _
                                             As Boolean


Public Declare Function PrintDeviceFont Lib "BXLLIB.dll" (ByVal nHorizontalPos As Long, _
                                              ByVal nVerticalPos As Long, _
                                              ByVal nFontName As Long, _
                                              ByVal nHorizontalMulti As Long, _
                                              ByVal nVerticalMulti As Long, _
                                              ByVal nRotation As Long, _
                                              ByVal bBold As Boolean, _
                                              ByVal szText As String) _
                                              As Boolean


Public Declare Function SetConfigOfPrinter Lib "BXLLIB.dll" (ByVal nSpeed As Long, _
                                                 ByVal nDensity As Long, _
                                                 ByVal nOrientation As Long, _
                                                 ByVal bAutoCut As Long, _
                                                 ByVal nCuttingPeriod As Long, _
                                                 ByVal bBackFeeding As Boolean) _
                                                 As Boolean


Public Declare Function Prints Lib "BXLLIB.dll" (ByVal nLabelSet As Long, _
                                     ByVal nCopiesOfEachLabel As Long) _
                                     As Boolean


Public Declare Function SetPaper Lib "BXLLIB.dll" (ByVal nHorizontalMagin As Long, _
                                       ByVal nVerticalMargin As Long, _
                                       ByVal nPaperWidth As Long, _
                                       ByVal nPaperLength As Long, _
                                       ByVal nMediaType As Long, _
                                       ByVal nOffSet As Long, _
                                       ByVal nGapLengthORThicknessOfBlackLine As Long) _
                                       As Boolean


Public Declare Function ClearBuffer Lib "BXLLIB.dll" () As Boolean


Public Declare Function PrintBlock Lib "BXLLIB.dll" (ByVal nHorizontalStartPos As Long, _
                                         ByVal nVerticalStartPos As Long, _
                                         ByVal nHorizontalEndPos As Long, _
                                         ByVal nVerticalEndPos As Long, _
                                         ByVal nOption As Long, _
                                         ByVal nThickness As Long) _
                                         As Boolean

'/******************************************************************************/
' Circle draw
' int nHorizontalStartPos   : X position
' int nVerticalStartPos     : Y position
' int nDiameter             : 원 Size Lib "BXLLIB.dll" (반지름) 1~6
' int nMulti                : 확대 Lib "BXLLIB.dll" (1~4)
'/******************************************************************************/

Public Declare Function PrintCircle Lib "BXLLIB.dll" (ByVal nHorizontalStartPos As Long, _
                                         ByVal nVerticalStartPos As Long, _
                                         ByVal nDiameter As Long, _
                                         ByVal nMulti As Long) _
                                         As Boolean


Public Declare Function PrintDirect Lib "BXLLIB.dll" (ByVal pDirectData As String) As Long


Public Declare Function StartLabel Lib "BXLLIB.dll" () As Boolean


Public Declare Sub EndLabel Lib "BXLLIB.dll" ()



Public Declare Function PrintTrueFontLib Lib "BXLLIB.dll" (ByVal nXPos As Long, _
                                ByVal nYPos As Long, _
                                ByVal strFontName As String, _
                                ByVal nFontSize As Long, _
                                ByVal nRotaion As Long, _
                                ByVal bItalic As Boolean, _
                                ByVal bBold As Boolean, _
                                ByVal bUnderline As Boolean, _
                                ByVal strText As String) _
                                As Boolean

Public Declare Function PrintTrueFontLibWithAlign Lib "BXLLIB.dll" (ByVal nXPos As Long, _
                                ByVal nYPos As Long, _
                                ByVal strFontName As String, _
                                ByVal nFontSize As Long, _
                                ByVal nRotaion As Long, _
                                ByVal bItalic As Boolean, _
                                ByVal bBold As Boolean, _
                                ByVal bUnderline As Boolean, _
                                ByVal strText As String, _
                                ByVal nPrintWidth As String, _
                                ByVal nAlignment As Long) _
                                As Boolean


Public Declare Function PrintVectorFont Lib "BXLLIB.dll" ( _
                                        ByVal nXPos As Long, _
                                        ByVal nYPos As Long, _
                                        ByVal strFontSelection As String, _
                                        ByVal nFontWidth As Long, _
                                        ByVal nFontHeight As Long, _
                                        ByVal strRightSideCharSpacing As String, _
                                        ByVal bBold As Boolean, _
                                        ByVal bReversePrinting As Boolean, _
                                        ByVal bTextStyle As Boolean, _
                                        ByVal nRotation As Long, _
                                        ByVal strTextAlignment As String, _
                                        ByVal nTextDirection As Long, _
                                        ByVal pData As String) _
                                        As Boolean


Public Declare Function PrintQRCode Lib "BXLLIB.dll" (ByVal nXPos As Long, _
                                                      ByVal nYPos As Long, _
                                                      ByVal nModel As Long, _
                                                      ByVal nECCLevel As Long, _
                                                      ByVal nSize As Long, _
                                                      ByVal nRotation As Long, _
                                                      ByVal pData As String) _
                                                      As Boolean

Public Declare Function SetShowMsgBox Lib "BXLLIB.dll" (ByVal bShowMsgBox As Boolean) As Boolean


Public Declare Function PrintImageLib Lib "BXLLIB.dll" (ByVal nHorizontalStartPos As Long, _
                                                        ByVal nVerticalStartPos As Long, _
                                                        ByVal pBitmapFilename As String, _
                                                        ByVal nDither As Long, _
                                                        ByVal bDataCompression As Boolean) _
                                                        As Boolean
                                                        
Public Declare Function PrintImageLibWithSize Lib "BXLLIB.dll" (ByVal nHorizontalStartPos As Long, _
                                                        ByVal nVerticalStartPos As Long, _
                                                        ByVal nNewWidth As Long, _
                                                        ByVal nNewHeight As Long, _
                                                        ByVal pBitmapFilename As String, _
                                                        ByVal nDither As Long, _
                                                        ByVal bDataCompression As Boolean) _
                                                        As Boolean




