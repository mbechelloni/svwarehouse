VERSION 5.00
Begin VB.Form frmMain 
   Caption         =   "Settings"
   ClientHeight    =   7860
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   6000
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00404040&
   LinkTopic       =   "Form1"
   ScaleHeight     =   7860
   ScaleWidth      =   6000
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      Height          =   3915
      Left            =   5820
      TabIndex        =   21
      Top             =   240
      Visible         =   0   'False
      Width           =   7935
      Begin VB.CommandButton btnPrint 
         Caption         =   "Print sample label"
         Height          =   855
         Left            =   300
         TabIndex        =   25
         Top             =   2700
         Width           =   3735
      End
      Begin VB.TextBox txtEvento 
         Height          =   435
         Left            =   240
         TabIndex        =   24
         Text            =   "Titolo Evento"
         Top             =   360
         Width           =   7515
      End
      Begin VB.TextBox txtDestination 
         Height          =   435
         Left            =   240
         TabIndex        =   23
         Text            =   "Destinazione"
         Top             =   960
         Width           =   7515
      End
      Begin VB.TextBox txtContenuto 
         Height          =   855
         Left            =   240
         MultiLine       =   -1  'True
         TabIndex        =   22
         Text            =   "frmMain.frx":0000
         Top             =   1620
         Width           =   7515
      End
      Begin VB.Label lblTest 
         AutoSize        =   -1  'True
         Caption         =   "Label4"
         BeginProperty Font 
            Name            =   "Myriad Pro Cond"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   4380
         TabIndex        =   26
         Top             =   3060
         Width           =   390
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Stampanti predefinite"
      Height          =   3915
      Left            =   240
      TabIndex        =   14
      Top             =   240
      Width           =   5415
      Begin VB.ComboBox cmbStandardPrinters 
         Height          =   360
         Left            =   360
         TabIndex        =   20
         Text            =   "Combo1"
         Top             =   2760
         Width           =   4815
      End
      Begin VB.ComboBox cmbPrtList2 
         Height          =   360
         Left            =   360
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   1740
         Width           =   4815
      End
      Begin VB.ComboBox cmbPrtList 
         Height          =   360
         Left            =   360
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   720
         Width           =   4815
      End
      Begin VB.Label Label6 
         Caption         =   "Stampante Lista materiale"
         Height          =   255
         Left            =   420
         TabIndex        =   19
         Top             =   2340
         Width           =   4695
      End
      Begin VB.Label Label5 
         Caption         =   "Stampante etichette uscita"
         Height          =   255
         Left            =   420
         TabIndex        =   18
         Top             =   1320
         Width           =   4695
      End
      Begin VB.Label Label4 
         Caption         =   "Stampante etichette inventario"
         Height          =   255
         Left            =   420
         TabIndex        =   16
         Top             =   300
         Width           =   4695
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Stampa Etichette per borse"
      Height          =   3435
      Left            =   240
      TabIndex        =   6
      Top             =   4380
      Width           =   5415
      Begin VB.TextBox Text7 
         Height          =   360
         Left            =   1260
         TabIndex        =   33
         Text            =   "1"
         Top             =   2520
         Width           =   615
      End
      Begin VB.TextBox Text6 
         Height          =   360
         Left            =   2100
         TabIndex        =   30
         Text            =   "1"
         Top             =   540
         Width           =   555
      End
      Begin VB.TextBox Text5 
         Height          =   360
         Left            =   1260
         TabIndex        =   28
         Text            =   "Vostro"
         Top             =   1440
         Width           =   2955
      End
      Begin VB.TextBox Text3 
         Height          =   360
         Left            =   1260
         TabIndex        =   12
         Text            =   "1"
         Top             =   1980
         Width           =   615
      End
      Begin VB.TextBox Text2 
         Height          =   360
         Left            =   1260
         TabIndex        =   10
         Text            =   "DELL"
         Top             =   960
         Width           =   2955
      End
      Begin VB.TextBox Text1 
         Height          =   360
         Left            =   1260
         TabIndex        =   8
         Text            =   "1"
         Top             =   540
         Width           =   495
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Stampa"
         Height          =   735
         Left            =   2760
         TabIndex        =   7
         Top             =   2100
         Width           =   2235
      End
      Begin VB.Label Label10 
         Caption         =   "Stampante etichette uscita"
         Height          =   255
         Left            =   900
         TabIndex        =   35
         Top             =   3060
         Width           =   3255
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Duplicati"
         BeginProperty Font 
            Name            =   "Myriad Pro Cond"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   34
         Top             =   2580
         Width           =   765
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Q.t�"
         BeginProperty Font 
            Name            =   "Myriad Pro Cond"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   32
         Top             =   1980
         Width           =   360
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "."
         BeginProperty Font 
            Name            =   "Myriad Pro Cond"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1860
         TabIndex        =   31
         Top             =   600
         Visible         =   0   'False
         Width           =   60
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Riga2"
         BeginProperty Font 
            Name            =   "Myriad Pro Cond"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   29
         Top             =   1440
         Width           =   495
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Riga1"
         BeginProperty Font 
            Name            =   "Myriad Pro Cond"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   11
         Top             =   1020
         Width           =   495
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Num"
         BeginProperty Font 
            Name            =   "Myriad Pro Cond"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   9
         Top             =   600
         Width           =   390
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Frame2"
      Height          =   3195
      Left            =   5820
      TabIndex        =   0
      Top             =   4380
      Visible         =   0   'False
      Width           =   2715
      Begin VB.TextBox Text4 
         Height          =   300
         Left            =   240
         TabIndex        =   27
         Text            =   "Text4"
         Top             =   3180
         Visible         =   0   'False
         Width           =   2115
      End
      Begin VB.TextBox txtLabel 
         Height          =   360
         Left            =   300
         TabIndex        =   13
         Text            =   "Text5"
         Top             =   2520
         Width           =   2175
      End
      Begin VB.CheckBox chkDouble 
         Caption         =   "Etichetta doppia"
         Height          =   240
         Left            =   420
         TabIndex        =   5
         Top             =   1080
         Width           =   2535
      End
      Begin VB.TextBox txtSuffisso 
         Height          =   360
         Left            =   360
         TabIndex        =   4
         Text            =   "DV"
         Top             =   480
         Width           =   555
      End
      Begin VB.TextBox txtEnd 
         Height          =   360
         Left            =   1800
         TabIndex        =   3
         Text            =   "1"
         Top             =   480
         Width           =   555
      End
      Begin VB.TextBox txtStart 
         Height          =   360
         Left            =   1140
         TabIndex        =   2
         Text            =   "1"
         Top             =   480
         Width           =   555
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Stampa"
         Height          =   435
         Left            =   300
         TabIndex        =   1
         Top             =   1500
         Width           =   1995
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private cmbLoading As Boolean


Private Sub cmbDensity_Change()

End Sub

Private Sub cmbPrtList2_Click()
If cmbLoading Then Exit Sub
If Me.cmbPrtList2.ListIndex > 0 Then
    PrinterUscita = Me.cmbPrtList2.List(Me.cmbPrtList2.ListIndex)
Else
    PrinterUscita = ""
End If
SaveSetting "StudioVisio", "MManager", "PrinterUscita", PrinterUscita
End Sub
Private Sub cmbPrtList_Click()
If cmbLoading Then Exit Sub
If Me.cmbPrtList.ListIndex > 0 Then
    PrinterInventario = Me.cmbPrtList.List(Me.cmbPrtList.ListIndex)
Else
    PrinterInventario = ""
End If
SaveSetting "StudioVisio", "MManager", "PrinterInventario", PrinterInventario
End Sub

Private Sub cmbStandardPrinters_Click()
If cmbLoading Then Exit Sub
If Me.cmbStandardPrinters.ListIndex > 0 Then
    PrinterStandard = Me.cmbStandardPrinters.List(Me.cmbStandardPrinters.ListIndex)
Else
    PrinterStandard = ""
End If
SaveSetting "StudioVisio", "MManager", "PrinterStandard", PrinterStandard
End Sub

Private Sub Command1_Click()


    Dim bResult As Boolean

    Dim strPrinterName As String
    
    Dim MM2D As Long
    
    Dim nPaper_Width As Long
    Dim nPaper_Height As Long
    Dim nMarginX As Long
    Dim nMarginY As Long

    Dim nSpeed As Long
    Dim nDensity As Long

    Dim bAutoCut As Boolean
    Dim bReverseFeeding As Boolean

    Dim nSensorType As Long
    
    Dim Direct_MaxiCode_2DBarcode_Cmd As String
    Dim Direct_PDF_2DBarcocde_Cmd As String
    Dim QRCode_data As String
    
   
        
    ' BIXOLON SLP-770II, BIXOLON SLP-770III
    ' BIXOLON SLP-D420, BIXOLON SLP-D423, BIXOLON SLP-T400, BIXOLON SLP-T403,
    ' BIXOLON SLP-TX400, BIXOLON SLP-TX403 ...
    ' ...
    
    
    
    For i = Val(txtStart.Text) To Val(Me.txtEnd.Text)
        
    
    
    
    
        
        
        strPrinterName = PrinterInventario 'cmbPrtList.Text
        If ConnectPrinter(strPrinterName) = False Then
            Exit Sub
        End If
        
        ' 203 DPI : 1mm = 8 dots
        ' 300 DPI : 1mm = 12 dots
        MM2D = IIf(GetPrinterResolution() < 300, 8, 12)
        
        nPaper_Width = CInt(73 * MM2D)
        nPaper_Height = CInt(18 * MM2D)
        nMarginX = CInt(0 * MM2D)
        nMarginY = CInt(0 * MM2D)
    
        nSpeed = 0
        nDensity = 14
    
    
        bReverseFeeding = 0
    
        nSensorType = GAP
       
            
        '   Set the label start
        bResult = StartLabel()
    
        '   Set Label and Printer
        'SetConfigOfPrinter(SPEED_50, 17, TOP, false, 0, true);
        bResult = SetConfigOfPrinter(nSpeed, nDensity, TOP, bAutoCut, 0, bReverseFeeding)
    
        '1 Inch : 25.4mm
        '1 mm   :  8 Dot in 203 DPI such as TX400, T400, D420, D220, SRP-770, SRP-770II
        '1 mm   : 12 Dot in 300 DPI such as TX403, T403, D423, D223
        '4 Inch : 25.4  * 4 * 8 = 812.8
        '6 Inch : 25.4  * 4 * 8 = 1219.2
    
        'SetPaper(16, 16, 813, 1220, GAP, 0, 16); ' 4 inch (Width) * 6 inch (Hiehgt)
        bResult = SetPaper(nMarginX, nMarginY, nPaper_Width, nPaper_Height, nSensorType, 0, 16) ' 4 inch (Width) * 6 inch (Hiehgt)
    
    
            PrintDirect ("STt")
    
    
        '   Clear Buffer of Printer
        bResult = ClearBuffer()
        
        
        '   Bar-code Type
'Public Const CODE39 As Long = 0
'Public Const CODE128 As Long = 1
'Public Const I2OF5 As Long = 2
'Public Const CODEBAR As Long = 3
'Public Const CODE93 As Long = 4
'Public Const UPC_A As Long = 5
'Public Const UPC_E As Long = 6
'Public Const EAN13 As Long = 7
'Public Const EAN8 As Long = 8
'Public Const UCC_EAN128 As Long = 9


       
        lblTest.Caption = Trim(txtLabel.Text)
        w = (lblTest.Width / 15)
        wn = (w - 4) / (87 - 4)
        nv = (16) - (16 - 4.5) * wn
        'MsgBox wn
        
        
        hgap = 38 * 8
        code = txtSuffisso.Text & " " & Format(i, "000")

       ' bResult = PrintTrueFontLib(12 * 8, 64, "Myriad Pro Cond", 13, 0, False, True, False, Trim(txtSuffisso.Text) & " " & Format(i, "000"))
        bResult = Print1DBarcode(2.5 * 8, 75, CODE39, 3, 6, 55, ROTATE_0, False, "*12345*")
        If chkDouble.Value = 0 Then
            i = i + 1
        End If
        code = txtSuffisso.Text & " " & Format(i, "000")
        bResult = PrintTrueFontLib(hgap + nv * 8, 110, "Myriad Pro Cond", 10, 0, False, True, False, Trim(txtLabel.Text))
        bResult = Print1DBarcode(hgap + 4.5 * 8, 65, CODE128, 3, 6, 45, ROTATE_0, False, "234865")
        
    
        '   Print Command
        bResult = Prints(1, 1)
    
        '   Set the Label End
        EndLabel
    
    
    
    Next i
    '   Disconnect Printer Driver
    bResult = DisconnectPrinter()
    Text4.SetFocus
    
    
End Sub

Private Sub Command2_Click()




 Dim bResult As Boolean

    Dim strPrinterName As String
    
    Dim MM2D As Long
    
    Dim nPaper_Width As Long
    Dim nPaper_Height As Long
    Dim nMarginX As Long
    Dim nMarginY As Long

    Dim nSpeed As Long
    Dim nDensity As Long

    Dim bAutoCut As Boolean
    Dim bReverseFeeding As Boolean

    Dim nSensorType As Long
    Dim myNum As Integer
    Dim myDuplicati As Integer
    Dim Direct_MaxiCode_2DBarcode_Cmd As String
    Dim j As Integer
   
    Label2.Caption = Text1.Text
    Label2.AutoSize = True
    
    Label3.Caption = Text2.Text
    Label3.AutoSize = True
    
   ' MsgBox Label2.Width / 7.91
        
    
    ' BIXOLON SLP-770II, BIXOLON SLP-770III
    ' BIXOLON SLP-D420, BIXOLON SLP-D423, BIXOLON SLP-T400, BIXOLON SLP-T403,
    ' BIXOLON SLP-TX400, BIXOLON SLP-TX403 ...
    ' ...
    
    
    
        For i = 1 To Val(Text3.Text)
            myNum = Val(Text6.Text) + i - 1
            myDuplicati = Val(Text7.Text)
            If myDuplicati < 1 Then myDuplicati = 1
            For j = 1 To myDuplicati
                strPrinterName = PrinterUscita 'cmbPrtList.Text
                If ConnectPrinter(strPrinterName) = False Then
                    Exit Sub
                End If
                
                ' 203 DPI : 1mm = 8 dots
                ' 300 DPI : 1mm = 12 dots
                MM2D = IIf(GetPrinterResolution() < 300, 8, 12)
                nPaper_Width = CInt(52 * MM2D) 'valore in mm
                nPaper_Height = CInt(18 * MM2D)
                nMarginX = CInt(0 * MM2D)
                nMarginY = CInt(0 * MM2D)
            
                nSpeed = 0
                nDensity = 14
                bReverseFeeding = 0
                nSensorType = GAP
                '   Set the label start
                bResult = StartLabel()
                '   Set Label and Printer
                'SetConfigOfPrinter(SPEED_50, 17, TOP, false, 0, true);
                bResult = SetConfigOfPrinter(nSpeed, nDensity, TOP, bAutoCut, 0, bReverseFeeding)
            
                '1 Inch : 25.4mm
                '1 mm   :  8 Dot in 203 DPI such as TX400, T400, D420, D220, SRP-770, SRP-770II
                '1 mm   : 12 Dot in 300 DPI such as TX403, T403, D423, D223
                '4 Inch : 25.4  * 4 * 8 = 812.8
                '6 Inch : 25.4  * 4 * 8 = 1219.2
            
                'SetPaper(16, 16, 813, 1220, GAP, 0, 16); ' 4 inch (Width) * 6 inch (Hiehgt)
                
                bResult = SetPaper(nMarginX, nMarginY, nPaper_Width, nPaper_Height, nSensorType, 0, 16) ' 4 inch (Width) * 6 inch (Hiehgt)
                PrintDirect ("STt")
                '   Clear Buffer of Printer
                bResult = ClearBuffer()
                'Prima riga
                bResult = PrintTrueFontLib(CInt(2 * MM2D), 55, "Myriad Pro Cond", 18, 0, False, True, False, Trim(Text2.Text))
                'SEconda riga
                bResult = PrintTrueFontLib(CInt(2 * MM2D), 100, "Myriad Pro Cond", 14, 0, False, True, False, Trim(Text5.Text))
                'Numero
                If Len(Trim(Text1.Text)) = 1 Then
                    bResult = PrintTrueFontLib(CInt(25 * MM2D), 0, "Myriad Pro Cond", 50, 0, False, True, False, Trim(Text1.Text) & "." & Format(myNum, "00"))
                Else
                    bResult = PrintTrueFontLib(CInt(20 * MM2D), 26, "Myriad Pro Cond", 44, 0, False, True, False, Trim(Text1.Text) & "." & Format(myNum, "00"))
                End If
                '   Print Command
                bResult = Prints(1, 1)
                '   Set the Label End
                EndLabel
        
            Next j
        
        Next i
    

    '   Disconnect Printer Driver
    bResult = DisconnectPrinter()
    
    
    
    
End Sub

Private Sub Form_Load()
    Dim AppVer As String
    InitControls
    AppVer = "4117"
    Me.Caption = "SV WareHouse ver. " & AppVer
End Sub

Private Sub InitControls()

    Dim i As Long
    Dim bResult As Boolean
    Dim sDllVersion As String * 256
    Dim dValue As Double
        
    If GetDllVersion(sDllVersion) Then
     '   lblDllVersion.Caption = "Version " + CStr(sDllVersion)
    Else
        lblDllVersion.Caption = "Unknown"
    End If
    
    
    btnPrint.Enabled = True
    'GetStdInstalledPrinter
    'GetInstalledPrinter
    
    PrinterStandard = GetStdInstalledPrinter(PrinterStandard)
    PrinterInventario = GetInstalledPrinter(cmbPrtList, PrinterInventario)
    PrinterUscita = GetInstalledPrinter(cmbPrtList2, PrinterUscita)

    '--------------------------------------------------------
    ' Paper Size
    ' Default 4x6 inch -> (4*25.4) x (6*25.4) mm
    dValue = 100

    dValue = 69

    


End Sub

Private Function GetInstalledPrinter(myCombobox As Object, myPrinter As String) As String
    
    Dim ssPrinters As String * 4096
    Dim sPrinterList() As String
    Dim iIndex As Long
    Dim sTemp As String
    Dim found As String
    
    found = ""
    cmbLoading = True
    myCombobox.Clear
    myCombobox.AddItem "Seleziona una stampante ..."
    If GetBIXOLON_PrinterList(ssPrinters) <= 0 Then
        GetInstalledPrinter = ""
    Else
        sTemp = AlphaNumericOnly(CStr(ssPrinters))
        sPrinterList() = Split(sTemp, "^")
        
        If UBound(sPrinterList) >= 0 Then
                        
            For iIndex = 0 To UBound(sPrinterList)
                myCombobox.AddItem sPrinterList(iIndex)
                If sPrinterList(iIndex) = myPrinter Then
                        myCombobox.ListIndex = iIndex + 1
                        found = myPrinter
                End If
            Next iIndex
            GetInstalledPrinter = found
        Else
            GetInstalledPrinter = ""
        End If
        If found = "" Then
            myCombobox.ListIndex = 0
        End If
    End If
    cmbLoading = False
    
End Function
Private Function GetStdInstalledPrinter(myPrinter As String) As String
    
    Dim stdPrinterList() As String
    Dim iIndex As Long
    Dim found As String
    
        cmbLoading = True
        cmbStandardPrinters.Clear
        found = ""
        cmbStandardPrinters.AddItem "Seleziona una stampante ..."
        For i = 1 To Printers.Count - 1
            cmbStandardPrinters.AddItem Printers(i).DeviceName
            If Printers(i).DeviceName = PrinterStandard Then
                cmbStandardPrinters.ListIndex = i
                found = PrinterStandard
            End If
        Next i
        cmbLoading = False
        If found = "" Then
        cmbStandardPrinters.ListIndex = 0
        PrinterStandard = ""
        SaveSetting "StudioVisio", "MManager", "PrinterStandard", ""
        End If
        GetStdInstalledPrinter = found
    
End Function

' Search - Installed BIXOLON Printers


' Print sample label
Private Sub btnPrint_Click()

    Dim bResult As Boolean

    Dim strPrinterName As String
    
    Dim MM2D As Long
    
    Dim nPaper_Width As Long
    Dim nPaper_Height As Long
    Dim nMarginX As Long
    Dim nMarginY As Long

    Dim nSpeed As Long
    Dim nDensity As Long

    Dim bAutoCut As Boolean
    Dim bReverseFeeding As Boolean

    Dim nSensorType As Long
    
    Dim Direct_MaxiCode_2DBarcode_Cmd As String
    Dim Direct_PDF_2DBarcocde_Cmd As String
    Dim QRCode_data As String
    
   
        
    ' BIXOLON SLP-770II, BIXOLON SLP-770III
    ' BIXOLON SLP-D420, BIXOLON SLP-D423, BIXOLON SLP-T400, BIXOLON SLP-T403,
    ' BIXOLON SLP-TX400, BIXOLON SLP-TX403 ...
    ' ...
    strPrinterName = PrinterUscita 'cmbPrtList.Text
    If ConnectPrinter(strPrinterName) = False Then
        Exit Sub
    End If
    
    ' 203 DPI : 1mm = 8 dots
    ' 300 DPI : 1mm = 12 dots
    MM2D = IIf(GetPrinterResolution() < 300, 8, 12)
    
    nPaper_Width = CInt(90 * MM2D)
    nPaper_Height = CInt(69 * MM2D)
    nMarginX = CInt(-15 * MM2D)
    nMarginY = CInt(0 * MM2D)

    nSpeed = 3
    nDensity = 14


    bReverseFeeding = False

    nSensorType = GAP
   
        
    '   Set the label start
    bResult = StartLabel()

    '   Set Label and Printer
    'SetConfigOfPrinter(SPEED_50, 17, TOP, false, 0, true);
    bResult = SetConfigOfPrinter(nSpeed, nDensity, TOP, bAutoCut, 0, bReverseFeeding)

    '1 Inch : 25.4mm
    '1 mm   :  8 Dot in 203 DPI such as TX400, T400, D420, D220, SRP-770, SRP-770II
    '1 mm   : 12 Dot in 300 DPI such as TX403, T403, D423, D223
    '4 Inch : 25.4  * 4 * 8 = 812.8
    '6 Inch : 25.4  * 4 * 8 = 1219.2

    'SetPaper(16, 16, 813, 1220, GAP, 0, 16); ' 4 inch (Width) * 6 inch (Hiehgt)
    bResult = SetPaper(nMarginX, nMarginY, nPaper_Width, nPaper_Height, nSensorType, 0, 16) ' 4 inch (Width) * 6 inch (Hiehgt)


        PrintDirect ("STt")


    '   Clear Buffer of Printer
    bResult = ClearBuffer()
    
    
    
    ' Print Logo
    bResult = PrintImageLib(0, 20, "logo.jpg", DITHER_1, False)

   '   Draw Lines
    bResult = PrintBlock(0, 98, 798, 100, LINE_OVER_WRITING, -1)

    bResult = PrintTrueFontLib(0, 110, "Myriad Pro Cond", 22, 0, False, True, False, txtEvento.Text)
    bResult = PrintTrueFontLib(0, 110 + 74, "Myriad Pro", 10, 0, False, False, False, Me.txtDestination.Text)
    
    bResult = PrintDeviceFont(0, 256, ENG_24X38, 1, 1, ROTATE_0, False, Me.txtContenuto.Text)
    
    

    'Print string using Vector Font
    '  P1 : Horizontal position (X) [dot]
    '  P2 : Vertical position (Y) [dot]
    '  P3 : Font selection
    '                  U: ASCII (1Byte code)
    '                  K: KS5601 (2Byte code)
    '                  B: BIG5 (2Byte code)
    '                  G: GB2312 (2Byte code)
    '                  J: Shift-JIS (2Byte code)
    ' P4  : Font width (W)[dot]
    ' P5  : Font height (H)[dot]
    ' P6  : Right-side character spacing [dot], Plus (+)/Minus (-) option can be used. Ex) 5, +3, -10
    ' P7  : Bold
    ' P8  : Reverse printing
    ' P9  : Text style  (N : Normal, I : Italic)
    ' P10 : Rotation (0 ~ 3)
    ' P11 : Text Alignment (Optional)
    ' P12 : Text string write direction
    ' P13 : data to print
    ' ※ : Third parameter, 'ASCII' must be set if Bixolon printer is SLP-T400, SLP-T403, SRP-770 and SRP-770II.
    
    bResult = PrintDeviceFont(608, 492, ENG_9X15, 1, 1, ROTATE_0, False, Now())

'    bResult = PrintDeviceFont(20, 174, ENG_24X38, 1, 1, ROTATE_0, False, "SHIP TO:")
'    bResult = PrintDeviceFont(20, 234, ENG_19X30, 1, 1, ROTATE_0, True, "BIXOLON")
'    bResult = PrintDeviceFont(20, 280, ENG_16X25, 1, 1, ROTATE_0, False, "7th FL, MiraeAsset Venture Tower,")
'    bResult = PrintDeviceFont(20, 310, ENG_16X25, 1, 1, ROTATE_0, False, "685, Sampyeong-dong, Bundang-gu,")
'    bResult = PrintDeviceFont(20, 340, ENG_16X25, 1, 1, ROTATE_0, False, "Seongnam-si, Gyeonggi-do,")
'    bResult = PrintDeviceFont(20, 370, ENG_16X25, 1, 1, ROTATE_0, False, "463-400, KOREA")
'
'    bResult = PrintDeviceFont(26, 421, ENG_12X20, 1, 1, ROTATE_0, False, "POSTAL CODE")
'    bResult = PrintDeviceFont(503, 798, ENG_12X20, 1, 1, ROTATE_0, False, "DESTINATION")
'    bResult = PrintDeviceFont(42, 841, ENG_32X50, 1, 1, ROTATE_0, True, "30 Kg")
'    bResult = PrintDeviceFont(25, 798, ENG_12X20, 1, 1, ROTATE_0, False, "WEIGHT:")
'    bResult = PrintDeviceFont(259, 798, ENG_12X20, 1, 1, ROTATE_0, False, "DELIVERY NO:")
'    bResult = PrintDeviceFont(23, 630, ENG_12X20, 1, 1, ROTATE_0, False, "AWB:")
'    bResult = PrintDeviceFont(274, 841, ENG_32X50, 1, 1, ROTATE_0, True, "425518")
'    bResult = PrintDeviceFont(104, 627, ENG_19X30, 1, 1, ROTATE_0, False, "8741493121")
'    bResult = PrintDeviceFont(565, 841, ENG_32X50, 1, 1, ROTATE_0, True, "ICN")
    
    
  

    '   Prints 1D Barcodes
    bResult = Print1DBarcode(30, 444, CODE39, 3, 6, 60, ROTATE_0, True, "HR420")


    





    '   Print Command
    bResult = Prints(1, 1)

    '   Set the Label End
    EndLabel
        
    '   Disconnect Printer Driver
    bResult = DisconnectPrinter()
            
End Sub

