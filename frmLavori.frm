VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "mscomctl.ocx"
Begin VB.Form frmLavori 
   Caption         =   "Lavori"
   ClientHeight    =   7440
   ClientLeft      =   60
   ClientTop       =   705
   ClientWidth     =   15645
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   7440
   ScaleWidth      =   15645
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCaricaCommesse 
      Caption         =   "Carica commessa"
      Height          =   435
      Left            =   120
      TabIndex        =   10
      Top             =   120
      Width           =   2175
   End
   Begin VB.Frame frmNuovoLavoro 
      Caption         =   "Modifica nome e luogo lavoro"
      Height          =   1095
      Left            =   120
      TabIndex        =   4
      Top             =   6180
      Width           =   15375
      Begin VB.CommandButton cmdDelete 
         Caption         =   "Cancella"
         Height          =   675
         Left            =   13740
         TabIndex        =   13
         Top             =   300
         Width           =   1515
      End
      Begin VB.TextBox txtCitta 
         Height          =   375
         Left            =   8220
         TabIndex        =   11
         Top             =   480
         Width           =   2535
      End
      Begin VB.TextBox txtLuogo 
         Height          =   375
         Left            =   4500
         TabIndex        =   7
         Top             =   480
         Width           =   3615
      End
      Begin VB.TextBox txtNomeLavoro 
         Height          =   375
         Left            =   120
         TabIndex        =   6
         Top             =   480
         Width           =   4275
      End
      Begin VB.CommandButton cmdNuovo 
         Caption         =   "Modifica"
         Height          =   675
         Left            =   10800
         TabIndex        =   5
         Top             =   300
         Width           =   1515
      End
      Begin VB.Label Label3 
         Caption         =   "Sede"
         Height          =   255
         Left            =   4560
         TabIndex        =   12
         Top             =   240
         Width           =   2055
      End
      Begin VB.Label Label2 
         Caption         =   "Citt�"
         Height          =   195
         Left            =   8280
         TabIndex        =   9
         Top             =   240
         Width           =   1755
      End
      Begin VB.Label Label1 
         Caption         =   "Nome"
         Height          =   255
         Left            =   180
         TabIndex        =   8
         Top             =   240
         Width           =   2055
      End
   End
   Begin VB.TextBox txtCerca 
      Height          =   315
      Left            =   11760
      TabIndex        =   3
      Top             =   180
      Width           =   2775
   End
   Begin VB.CommandButton cmdCerca 
      Caption         =   "Cerca"
      Height          =   315
      Left            =   14640
      TabIndex        =   2
      Top             =   180
      Width           =   735
   End
   Begin VB.CheckBox chkCancellati 
      Caption         =   "Mostra cancellati"
      Height          =   255
      Left            =   9840
      TabIndex        =   1
      Top             =   180
      Width           =   1875
   End
   Begin MSComctlLib.ListView lvLavori 
      Height          =   5475
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   15375
      _ExtentX        =   27120
      _ExtentY        =   9657
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuSettings 
         Caption         =   "Settings"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
      End
   End
   Begin VB.Menu mnuMateriali 
      Caption         =   "Materiali"
      Begin VB.Menu mnuGestMateriali 
         Caption         =   "Gestione Materiali"
      End
   End
End
Attribute VB_Name = "frmLavori"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdCaricaCommesse_Click()
frmEvents.Show 1
End Sub


Private Sub cmdCerca_Click()
    loadLavori Trim(txtCerca.Text), 0
End Sub

Private Sub cmdDelete_Click()
    Dim mySQL As String
    Dim myRS As New ADODB.Recordset
    Dim ret As Integer
    
    On Error GoTo hell
    ret = MsgBox("Vuoi cancellare il record selezionato?", vbQuestion + vbYesNo, Me.Caption)
    If ret = vbNo Then Exit Sub

    openMysqlDB ("cmdDelete_Click")
    mySQL = "UPDATE lavori SET del='" & Format(Now, "yyyymmddhhss") & "'"
    mySQL = mySQL & " WHERE IDLavoro = " & Val(lvLavori.selectedItem.Text)
    myDB.Execute mySQL
    closeMysqlDB ("cmdDelete_Click")
    loadLavori "", 0
   Exit Sub
hell:
End Sub

Private Sub cmdNuovo_Click()
    Dim mySQL As String
    Dim myRS As New ADODB.Recordset
    Dim ret As Integer
    
    On Error GoTo hell
    ret = MsgBox("Vuoi aggiornare il record selezionato?", vbQuestion + vbYesNo, Me.Caption)
    If ret = vbNo Then Exit Sub
    If Trim(txtNomeLavoro.Text) = "" Then Exit Sub
    If Trim(txtCitta.Text) = "" Then Exit Sub

    openMysqlDB ("cmdNuovo_Click")
    mySQL = "UPDATE lavori SET Nome='" & cleanSQL(Trim(txtNomeLavoro.Text)) & "',"
    mySQL = mySQL & "Sede = '" & cleanSQL(Trim(txtLuogo.Text)) & "',"
    mySQL = mySQL & "Luogo = '" & cleanSQL(Trim(txtCitta.Text)) & "'"
    mySQL = mySQL & " WHERE IDLavoro = " & Val(lvLavori.selectedItem.Text)
    myDB.Execute mySQL
    loadLavori "", Val(lvLavori.selectedItem.Text)
    closeMysqlDB ("cmdNuovo_Click")
    
'    If Trim(txtNomeLavoro.Text) <> "" And Trim(txtLuogo.Text) <> "" Then
'        openMysqlDB ("loadLavori")
'        mySQL = "INSERT INTO lavori ("
'        mySQL = mySQL & "nome,"
'        mySQL = mySQL & "luogo,"
'        mySQL = mySQL & "createTS "
'
'        mySQL = mySQL & " ) VALUES ( "
'
'        mySQL = mySQL & "'" & cleanSQL(txtNomeLavoro.Text) & "',"
'        mySQL = mySQL & "'" & cleanSQL(txtLuogo.Text) & "'," 'luogo
'        mySQL = mySQL & "'" & Format(Now(), "YYYYMMDDhhmmss") & "' )"
'        myDB.Execute mySQL
'        closeMysqlDB ("loadLavori")
'        loadLavori "", 0
'    End If
   Exit Sub
hell:
    
End Sub

Private Sub Form_Load()

    Dim i As Integer
    '
    
    lvLavori.ListItems.Clear
    lvLavori.HideColumnHeaders = False
    lvLavori.View = lvwReport
    lvLavori.ColumnHeaders.Clear
    lvLavori.ColumnHeaders.Add , , "ID", 400, lvwColumnLeft
    lvLavori.ColumnHeaders.Add , , "Nome", 3200, lvwColumnLeft
    lvLavori.ColumnHeaders.Add , , "Sede", 2800, lvwColumnLeft
    lvLavori.ColumnHeaders.Add , , "Citt�", 1600, lvwColumnLeft
    lvLavori.ColumnHeaders.Add , , "Inizio", 1200, lvwColumnLeft
    lvLavori.ColumnHeaders.Add , , "Fine", 1200, lvwColumnLeft
    lvLavori.ColumnHeaders.Add , , "IDCommessa", 800, lvwColumnLeft
    lvLavori.ColumnHeaders.Add , , "Cliente", 2800, lvwColumnLeft
    lvLavori.ColumnHeaders.Add , , "Time Stamp", 1200, lvwColumnLeft
    For i = 1 To lvLavori.ColumnHeaders.Count
        lvLavori.ColumnHeaders(i).Tag = "A"
    Next i
    
    loadLavori "", 0
    
End Sub

Private Sub lvLavori_AfterLabelEdit(Cancel As Integer, NewString As String)
Cancel = 1
End Sub

Private Sub lvLavori_BeforeLabelEdit(Cancel As Integer)
Cancel = 1
End Sub

Private Sub lvLavori_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    If lvLavori.ColumnHeaders(ColumnHeader.Index).Tag = "A" Then
        lvLavori.ColumnHeaders(ColumnHeader.Index).Tag = "D"
        lvLavori.SortOrder = lvwAscending
    Else
        lvLavori.ColumnHeaders(ColumnHeader.Index).Tag = "A"
        lvLavori.SortOrder = lvwDescending
    End If
    
    lvLavori.Sorted = True
    lvLavori.SortKey = ColumnHeader.Index - 1
End Sub
Public Sub loadLavori(search As String, myIDCommessa As Integer)



    Dim myRS As New ADODB.Recordset
    Dim mySQL As String
    Dim prezzoTotale As Single
    Dim printedLabels As Integer
    Dim myIsNullCondition As String
    Dim mySelectedIDCommessa As Integer
    openMysqlDB ("loadLavori")
    

    myRS.CursorLocation = adUseClient
    myRS.CursorType = adOpenKeyset
    
    If chkCancellati = 0 Then myIsNullCondition = "AND del = '' "
    Select Case search

    Case ""
        mySQL = "SELECT * FROM lavori WHERE 1=1 " & myIsNullCondition & "ORDER BY IDLavoro ASC"
    Case Else
        mySQL = "SELECT * FROM lavori WHERE (nome LIKE '%" & search & "%' OR createTS LIKE '%" & search & "%' OR luogo LIKE '%" & search & "%' OR sede LIKE '%" & search & "%') " & myIsNullCondition & "ORDER BY IDlavoro ASC"

    End Select
    lvLavori.ListItems.Clear
    lvLavori.Sorted = False
    myRS.Open mySQL, myDB, adOpenForwardOnly, adLockReadOnly
    While Not myRS.EOF
        lvLavori.ListItems.Add , "A" & myRS("IDlavoro"), myRS("IDlavoro")
        lvLavori.ListItems(lvLavori.ListItems.Count).SubItems(1) = "" & myRS("nome")
        lvLavori.ListItems(lvLavori.ListItems.Count).SubItems(2) = "" & myRS("sede")
        lvLavori.ListItems(lvLavori.ListItems.Count).SubItems(3) = "" & myRS("luogo")
        lvLavori.ListItems(lvLavori.ListItems.Count).SubItems(4) = Format(formatDate("" & myRS("inizio")), "d mmm yyyy")
        lvLavori.ListItems(lvLavori.ListItems.Count).SubItems(5) = Format(formatDate("" & myRS("fine")), "d mmm yyyy")
        mySelectedIDCommessa = Val("" & myRS("IDCommessa"))
        lvLavori.ListItems(lvLavori.ListItems.Count).SubItems(6) = mySelectedIDCommessa
        lvLavori.ListItems(lvLavori.ListItems.Count).SubItems(7) = "" & myRS("cliente")
        lvLavori.ListItems(lvLavori.ListItems.Count).SubItems(8) = "" & myRS("createTS")
        If mySelectedIDCommessa = myIDCommessa Then
            lvLavori.ListItems(lvLavori.ListItems.Count).Selected = True
            lvLavori.ListItems(lvLavori.ListItems.Count).EnsureVisible
        End If


        myRS.MoveNext
    Wend
    'lblArtCount.Caption = lvLavori.ListItems.Count & "/" & printedLabels & " Articoli/Etichette - Prezzo tot " & prezzoTotale
    
    closeMysqlDB ("loadLavori")
 

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Cancel = 1
    mnuExit_Click
End Sub

Private Sub lvLavori_Click()

    If lvLavori.ListItems.Count = 0 Then Exit Sub
    txtNomeLavoro.Text = lvLavori.selectedItem.SubItems(1)
    txtLuogo.Text = lvLavori.selectedItem.SubItems(2)
    txtCitta.Text = lvLavori.selectedItem.SubItems(3)
End Sub

Private Sub lvLavori_DblClick()
    frmMovimenti.myIDLavoro = Val(lvLavori.selectedItem)
    frmMovimenti.myNomeLavoro = lvLavori.selectedItem.SubItems(1)
    frmMovimenti.myLuogoLavoro = lvLavori.selectedItem.SubItems(2) & " - " & lvLavori.selectedItem.SubItems(3)
    frmMovimenti.myDate = lvLavori.selectedItem.SubItems(4) & " - " & lvLavori.selectedItem.SubItems(5)
    frmMovimenti.Show 1
    
End Sub

Private Sub mnuExit_Click()
        Dim ret As Integer
        ret = MsgBox("Vuoi uscire dal programma", vbQuestion + vbYesNo, Me.Caption)
        If ret = vbNo Then Exit Sub
        End
End Sub

Private Sub mnuGestMateriali_Click()
    frmMateriale.Show 1
End Sub

Private Sub mnuSettings_Click()
frmMain.Show
End Sub

