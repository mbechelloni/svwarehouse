VERSION 5.00
Begin VB.Form frmMovDet 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Movimenti - Dettaglio"
   ClientHeight    =   7470
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   5085
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7470
   ScaleWidth      =   5085
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "Annulla"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   800
      Left            =   240
      TabIndex        =   13
      Top             =   6540
      Width           =   1600
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   800
      Left            =   3180
      TabIndex        =   12
      Top             =   6600
      Width           =   1600
   End
   Begin VB.Frame Frame1 
      Height          =   5300
      Left            =   300
      TabIndex        =   1
      Top             =   1140
      Width           =   4500
      Begin VB.CommandButton cmdReset 
         BackColor       =   &H000000FF&
         Caption         =   "C"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   27.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1000
         Left            =   240
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   4080
         Width           =   1000
      End
      Begin VB.CommandButton Command1 
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   27.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1000
         Index           =   0
         Left            =   1740
         TabIndex        =   11
         Top             =   4080
         Width           =   1000
      End
      Begin VB.CommandButton Command1 
         Caption         =   "9"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   27.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1000
         Index           =   9
         Left            =   3120
         TabIndex        =   10
         Top             =   2880
         Width           =   1000
      End
      Begin VB.CommandButton Command1 
         Caption         =   "8"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   27.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1000
         Index           =   8
         Left            =   1740
         TabIndex        =   9
         Top             =   2880
         Width           =   1000
      End
      Begin VB.CommandButton Command1 
         Caption         =   "7"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   27.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1000
         Index           =   7
         Left            =   300
         TabIndex        =   8
         Top             =   2880
         Width           =   1000
      End
      Begin VB.CommandButton Command1 
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   27.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1000
         Index           =   6
         Left            =   3240
         TabIndex        =   7
         Top             =   1560
         Width           =   1000
      End
      Begin VB.CommandButton Command1 
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   27.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1000
         Index           =   5
         Left            =   1800
         TabIndex        =   6
         Top             =   1560
         Width           =   1000
      End
      Begin VB.CommandButton Command1 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   27.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1000
         Index           =   4
         Left            =   240
         TabIndex        =   5
         Top             =   1560
         Width           =   1000
      End
      Begin VB.CommandButton Command1 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   27.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1000
         Index           =   3
         Left            =   3240
         TabIndex        =   4
         Top             =   360
         Width           =   1000
      End
      Begin VB.CommandButton Command1 
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   27.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1000
         Index           =   2
         Left            =   1800
         TabIndex        =   3
         Top             =   300
         Width           =   1000
      End
      Begin VB.CommandButton Command1 
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   27.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1000
         Index           =   1
         Left            =   240
         TabIndex        =   2
         Top             =   300
         Width           =   1000
      End
   End
   Begin VB.TextBox txtQuantita 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   27.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   300
      MaxLength       =   3
      TabIndex        =   0
      Top             =   180
      Width           =   4455
   End
End
Attribute VB_Name = "frmMovDet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public myValue As Integer
Private FirstClick As Boolean

Private Sub cmdAnnulla_Click()
frmMovimenti.myQuantita = 0
Unload Me
End Sub

Private Sub cmdOK_Click()
Dim ret As Integer

If Val(txtQuantita.Text) < 1 Or Val(txtQuantita.Text) > 999 Then
    MsgBox "Attenzione immetere un numero valido!", vbInformation, Me.Caption
    Exit Sub
End If

frmMovimenti.myQuantita = Val(txtQuantita.Text)
Unload Me
End Sub

Private Sub cmdReset_Click()
    txtQuantita.Text = ""
End Sub

Private Sub Command1_Click(Index As Integer)
If FirstClick Then
    txtQuantita.Text = Index
    FirstClick = False
Else
    txtQuantita.Text = txtQuantita.Text & Index
End If
End Sub

Private Sub Form_Load()
    FirstClick = True
    txtQuantita.Text = myValue
    If Len(myValue) > 0 Then
        txtQuantita.SelStart = 0
        txtQuantita.SelLength = 1000
    End If
End Sub
