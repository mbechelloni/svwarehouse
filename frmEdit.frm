VERSION 5.00
Begin VB.Form frmEdit 
   Caption         =   "Edit"
   ClientHeight    =   8535
   ClientLeft      =   60
   ClientTop       =   405
   ClientWidth     =   11610
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   8535
   ScaleWidth      =   11610
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbTipo 
      Height          =   315
      Left            =   6600
      TabIndex        =   39
      Text            =   "Tipologia"
      Top             =   4980
      Width           =   4815
   End
   Begin VB.Frame Frame3 
      Caption         =   "DisponibilitÓ dell'articolo"
      Height          =   975
      Left            =   180
      TabIndex        =   34
      Top             =   5640
      Width           =   11235
      Begin VB.TextBox txtNotaDisp 
         Height          =   315
         Left            =   4740
         TabIndex        =   38
         Top             =   420
         Width           =   6315
      End
      Begin VB.ComboBox cmbStato_Disp 
         Height          =   315
         Left            =   2100
         TabIndex        =   36
         Text            =   "Stato disponibilitÓ"
         Top             =   420
         Width           =   1815
      End
      Begin VB.Label Label15 
         Caption         =   "Nota"
         Height          =   255
         Left            =   4200
         TabIndex        =   37
         Top             =   480
         Width           =   435
      End
      Begin VB.Label Label13 
         Caption         =   "Stato della disponibilitÓ"
         Height          =   255
         Left            =   180
         TabIndex        =   35
         Top             =   420
         Width           =   1875
      End
   End
   Begin VB.ComboBox cmbMagazzini 
      Height          =   315
      Left            =   6600
      TabIndex        =   33
      Text            =   "magazzino"
      Top             =   4320
      Width           =   4815
   End
   Begin VB.TextBox txtProgressivo 
      Height          =   285
      Left            =   6600
      TabIndex        =   30
      Top             =   720
      Width           =   1395
   End
   Begin VB.ComboBox cmbCategoria 
      Height          =   315
      Left            =   6600
      TabIndex        =   28
      Text            =   "Combo1"
      Top             =   240
      Width           =   4815
   End
   Begin VB.Frame Frame1 
      Caption         =   "Opzioni inserimento"
      Height          =   855
      Left            =   180
      TabIndex        =   22
      Top             =   6780
      Width           =   11235
      Begin VB.TextBox txtNumDulicati 
         Height          =   285
         Left            =   2040
         TabIndex        =   24
         Text            =   "1"
         Top             =   300
         Width           =   1095
      End
      Begin VB.TextBox txtLabelNum 
         Height          =   285
         Left            =   5340
         TabIndex        =   23
         Top             =   300
         Width           =   1095
      End
      Begin VB.Label Label9 
         Caption         =   "n░ record da inserire"
         Height          =   195
         Left            =   240
         TabIndex        =   26
         Top             =   360
         Width           =   1695
      End
      Begin VB.Label Label10 
         Caption         =   "label con numerazione progressiva da:"
         Height          =   435
         Left            =   3540
         TabIndex        =   25
         Top             =   240
         Width           =   1695
      End
   End
   Begin VB.CommandButton cmdScorporaIVA 
      Caption         =   "Scorpora IVA"
      Height          =   375
      Left            =   3420
      TabIndex        =   21
      Top             =   4380
      Width           =   1215
   End
   Begin VB.CommandButton cmdCancella 
      Caption         =   "Elimina articolo"
      Height          =   555
      Left            =   120
      TabIndex        =   20
      Top             =   7800
      Width           =   1635
   End
   Begin VB.TextBox txtNote 
      Height          =   1335
      Left            =   6600
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   18
      Top             =   2580
      Width           =   4815
   End
   Begin VB.TextBox txtSeriale 
      Height          =   435
      Left            =   1620
      TabIndex        =   16
      Top             =   3660
      Width           =   3075
   End
   Begin VB.CommandButton cmdDuplica 
      Caption         =   "Duplica articolo"
      Height          =   555
      Left            =   1980
      TabIndex        =   15
      Top             =   7800
      Width           =   1635
   End
   Begin VB.CommandButton cmdAnnulla 
      Caption         =   "Annulla"
      Height          =   555
      Left            =   10020
      TabIndex        =   14
      Top             =   7800
      Width           =   1395
   End
   Begin VB.CommandButton cmdSalva 
      Caption         =   "Salva"
      Height          =   555
      Left            =   8340
      TabIndex        =   13
      Top             =   7800
      Width           =   1395
   End
   Begin VB.TextBox txtPrezzo 
      Height          =   435
      Left            =   1620
      TabIndex        =   11
      Top             =   4320
      Width           =   1635
   End
   Begin VB.TextBox txtDescrizione 
      Height          =   1035
      Left            =   6600
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   9
      Top             =   1320
      Width           =   4815
   End
   Begin VB.TextBox txtModello 
      Height          =   435
      Left            =   1620
      TabIndex        =   7
      Top             =   2940
      Width           =   3075
   End
   Begin VB.TextBox txtMarca 
      Height          =   435
      Left            =   1620
      TabIndex        =   5
      Top             =   2280
      Width           =   3075
   End
   Begin VB.TextBox txtLabel 
      Height          =   435
      Left            =   1620
      TabIndex        =   3
      Top             =   1500
      Width           =   3075
   End
   Begin VB.TextBox txtCodice 
      Height          =   435
      Left            =   1620
      TabIndex        =   1
      Top             =   840
      Width           =   3015
   End
   Begin VB.Label Label16 
      Caption         =   "Tipologia "
      Height          =   195
      Left            =   5160
      TabIndex        =   40
      Top             =   5040
      Width           =   1275
   End
   Begin VB.Label Label14 
      Caption         =   "Magazzino di appartenenza"
      Height          =   435
      Left            =   5160
      TabIndex        =   32
      Top             =   4260
      Width           =   1275
   End
   Begin VB.Label Label12 
      Caption         =   "progressivo"
      Height          =   255
      Left            =   5220
      TabIndex        =   31
      Top             =   720
      Width           =   1275
   End
   Begin VB.Label lblDel 
      Caption         =   "Cancellato:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   435
      Left            =   3840
      TabIndex        =   29
      Top             =   7860
      Width           =   4275
   End
   Begin VB.Label Label11 
      Caption         =   "categoria"
      Height          =   255
      Left            =   5160
      TabIndex        =   27
      Top             =   240
      Width           =   1275
   End
   Begin VB.Label Label8 
      Caption         =   "Note"
      Height          =   255
      Left            =   5160
      TabIndex        =   19
      Top             =   2640
      Width           =   1275
   End
   Begin VB.Label Label7 
      Caption         =   "Seriale"
      Height          =   255
      Left            =   180
      TabIndex        =   17
      Top             =   3660
      Width           =   1275
   End
   Begin VB.Label Label6 
      Caption         =   "Prezzo Netto"
      Height          =   255
      Left            =   180
      TabIndex        =   12
      Top             =   4380
      Width           =   1275
   End
   Begin VB.Label Label5 
      Caption         =   "Descrizione"
      Height          =   255
      Left            =   5220
      TabIndex        =   10
      Top             =   1320
      Width           =   1275
   End
   Begin VB.Label Label4 
      Caption         =   "Modello"
      Height          =   255
      Left            =   180
      TabIndex        =   8
      Top             =   2940
      Width           =   1275
   End
   Begin VB.Label Label3 
      Caption         =   "Marca"
      Height          =   255
      Left            =   180
      TabIndex        =   6
      Top             =   2340
      Width           =   1275
   End
   Begin VB.Label Label2 
      Caption         =   "Label"
      Height          =   255
      Left            =   180
      TabIndex        =   4
      Top             =   1680
      Width           =   1275
   End
   Begin VB.Label Label1 
      Caption         =   "Codice"
      Height          =   255
      Left            =   180
      TabIndex        =   2
      Top             =   960
      Width           =   1275
   End
   Begin VB.Label lblID 
      Height          =   255
      Left            =   180
      TabIndex        =   0
      Top             =   240
      Width           =   1275
   End
End
Attribute VB_Name = "frmEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public searchID As Integer
Private myNumerabile As Integer
Private myKit As Integer
Private isDeleted As Boolean

Private Sub cmdAnnulla_Click()
Unload Me
End Sub

Private Sub cmdCancella_Click()
    If cmdCancella.Tag <> "" Then
        saveData searchID, "UND"
    Else
        saveData searchID, "DEL"
    End If
End Sub

Private Sub cmdDuplica_Click()
  If validateFields Then saveData searchID, "DUP"
End Sub



Private Sub cmdSalva_Click()
    If validateFields Then saveData searchID, ""
End Sub

Private Sub cmdScorporaIVA_Click()

    Dim prezzo As Single
    
    prezzo = Val(txtPrezzo.Text)
    prezzo = prezzo / 1.22
    txtPrezzo.Text = Replace(Format(prezzo, ".00"), ",", ".")

End Sub

Private Sub Form_Load()
    
If searchID <> 0 Then 'modifica/cancella/duplica
    frmEdit.Caption = "Modifica/Duplica articolo"
    loadCategorie
    loadMagazzini
    loadStatoDisp
    loadTipo
    loadData (searchID)

    cmdSalva.Enabled = True
    cmdDuplica.Enabled = True
Else 'inserisci nuovo
    loadCategorie
    loadMagazzini
    loadStatoDisp
    loadTipo
    frmEdit.Caption = "Inserimento nuovo articolo"
    cmdDuplica.Enabled = False
    cmdCancella.Enabled = False
End If
If isDeleted Then
    cmdCancella.Tag = "Ripristina"
    cmdCancella.Caption = "Ripristina"
    lblDel.Visible = True
Else
    cmdCancella.Tag = ""
    cmdCancella.Caption = "Elimina"
    lblDel.Visible = False
End If
txtLabelNum.Enabled = False
Label10.Enabled = False

End Sub
Private Sub loadCategorie()
    Dim myRS As New ADODB.Recordset
    Dim mySQL As String
    openMysqlDB ("loadCategorie")
    cmbCategoria.Clear
    mySQL = "SELECT * FROM gruppiMateriale WHERE idParent=0"
    myRS.Open mySQL, myDB, adOpenForwardOnly, adLockReadOnly
    While Not myRS.EOF
        cmbCategoria.AddItem "" & myRS("descrizione")
        cmbCategoria.ItemData(cmbCategoria.NewIndex) = myRS("idGruppoMateriale")
    myRS.MoveNext
    Wend
    myRS.Close
    cmbCategoria.ListIndex = 0
    closeMysqlDB "loadCategorie"
End Sub
Private Sub loadMagazzini()
    Dim myRS As New ADODB.Recordset
    Dim mySQL As String
    openMysqlDB ("loadMagazzini")
    cmbMagazzini.Clear
    cmbMagazzini.AddItem "--nessuno--"
    cmbMagazzini.ItemData(cmbMagazzini.NewIndex) = 0
    mySQL = "SELECT * FROM magazzini"
    myRS.Open mySQL, myDB, adOpenForwardOnly, adLockReadOnly
    While Not myRS.EOF
        cmbMagazzini.AddItem "" & myRS("mg_nome")
        cmbMagazzini.ItemData(cmbMagazzini.NewIndex) = myRS("id_mg")
    myRS.MoveNext
    Wend
    myRS.Close
    cmbMagazzini.ListIndex = 0
    closeMysqlDB "loadMagazzini"
End Sub
Private Sub loadStatoDisp()
    Dim myRS As New ADODB.Recordset
    Dim mySQL As String
    openMysqlDB ("loadStatoDisp")
    cmbStato_Disp.Clear
    mySQL = "SELECT * FROM stato_disp"
    myRS.Open mySQL, myDB, adOpenForwardOnly, adLockReadOnly
    While Not myRS.EOF
        cmbStato_Disp.AddItem "" & myRS("sd_stato")
        cmbStato_Disp.ItemData(cmbStato_Disp.NewIndex) = myRS("id_sd")
    myRS.MoveNext
    Wend
    myRS.Close
    cmbStato_Disp.ListIndex = 0
    closeMysqlDB "loadStatoDisp"
End Sub
Private Sub loadTipo()
    Dim myRS As New ADODB.Recordset
    Dim mySQL As String
    openMysqlDB ("loadTipo")
    cmbTipo.Clear
    mySQL = "SELECT * FROM tipo"
    myRS.Open mySQL, myDB, adOpenForwardOnly, adLockReadOnly
    While Not myRS.EOF
        cmbTipo.AddItem "" & myRS("t_tipo")
        cmbTipo.ItemData(cmbTipo.NewIndex) = myRS("id_t")
    myRS.MoveNext
    Wend
    myRS.Close
    cmbTipo.ListIndex = 0
    closeMysqlDB "loadTipo"
End Sub
Private Sub loadData(myID As Integer)

    Dim myRS As New ADODB.Recordset
    Dim mySQL As String
    Dim i As Integer
    Dim myTipo As String
    Dim myDelDate As String
    openMysqlDB ("loadData")
 
    myRS.CursorLocation = adUseClient
    myRS.CursorType = adOpenKeyset
    mySQL = "SELECT * FROM materiale WHERE IDMateriale=" & myID

    myRS.Open mySQL, myDB, adOpenForwardOnly, adLockReadOnly
    If Not myRS.EOF Then
    
        lblID.Caption = Format(myRS("IDMateriale"), "00000")
        txtCodice.Text = "" & myRS("codice")
        txtLabel.Text = "" & myRS("label")
        txtMarca.Text = "" & myRS("marca")
        txtModello.Text = "" & myRS("modello")
        txtDescrizione.Text = "" & myRS("descrizione")
        txtProgressivo.Text = "" & myRS("progressivo")
        txtNote.Text = "" & myRS("note")
        txtSeriale.Text = "" & myRS("seriale")
        txtPrezzo.Text = Replace("" & myRS("prezzo"), ",", ".")
        myDelDate = "" & myRS("del")
        lblDel.Caption = "Canc: " & myDelDate
        isDeleted = IIf(myDelDate = "", False, True)
        For i = 0 To Me.cmbCategoria.ListCount - 1
            If Me.cmbCategoria.ItemData(i) = myRS("idGruppoMateriale") Then
                cmbCategoria.ListIndex = i
                Exit For
            End If
        Next i
        For i = 0 To Me.cmbMagazzini.ListCount - 1
            If Me.cmbMagazzini.ItemData(i) = myRS("mg_id") Then
                cmbMagazzini.ListIndex = i
                Exit For
            End If
        Next i
        For i = 0 To Me.cmbStato_Disp.ListCount - 1
            If Me.cmbStato_Disp.ItemData(i) = myRS("sd_id") Then
                cmbStato_Disp.ListIndex = i
                Exit For
            End If
        Next i
        txtNotaDisp.Text = "" & myRS("nota_sd")
        For i = 0 To Me.cmbTipo.ListCount - 1
            If Me.cmbTipo.ItemData(i) = myRS("t_id") Then
                cmbTipo.ListIndex = i
                Exit For
            End If
        Next i
        End If

    closeMysqlDB "loadData"
End Sub

Private Sub saveData(myID As Integer, myFlg As String)

    Dim mySQL As String
    Dim myRS As New ADODB.Recordset
    Dim lastUpdate As String
    Dim ret As Integer
    Dim message As String
    Dim myAction As String
    Dim i As Integer
    lastUpdate = Format(Now(), "YYYYMMDDhhmmss")
    Dim isMultiple As Boolean
    Dim myNumInsert As Integer
    Dim myLabel As String
    Dim myTipo As String
    myNumInsert = Val(txtNumDulicati.Text)
    If myNumInsert = 0 Then myNumInsert = 1
    If myNumInsert > 1 Then
        isMultiple = True
    End If
    
    If searchID <> 0 And myFlg = "" Then
        message = "Confermi le modifiche effetuate?"
        myAction = "U"
    ElseIf searchID = 0 And myFlg = "" Then
        If isMultiple Then
            message = "Confermi l'inserimento di " & Val(txtNumDulicati) & " nuovi articoli?"
            myAction = "N"
        Else
            message = "Confermi l'inserimento di un nuovo articolo?"
            myAction = "N"
        End If
    ElseIf searchID <> 0 And myFlg = "DEL" Then
        message = "Confermi la cancellazione dell'articolo?"
        myAction = "D"
    ElseIf searchID <> 0 And myFlg = "UND" Then
        message = "Vuoi ripristinare l'articolo?"
        myAction = "R"
    ElseIf searchID <> 0 And myFlg = "DUP" Then
        If isMultiple Then
            message = "Confermi l'inserimento di " & Val(txtNumDulicati) & " nuovi articoli duplicati?"
            myAction = "N"
        Else
            message = "Confermi l'inserimento di un nuovo articolo duplicato?"
            myAction = "N"
        End If
    End If
    ret = MsgBox(message, vbQuestion + vbYesNo, Me.Caption)
    If ret = vbNo Then Exit Sub
 
    openMysqlDB ("saveData")
    Select Case myAction
        Case "D"
            mySQL = "UPDATE materiale SET del='" & lastUpdate & "' WHERE IDMateriale=" & myID
            myDB.Execute mySQL
        Case "R"
            mySQL = "UPDATE materiale SET del= NULL WHERE IDMateriale=" & myID
            myDB.Execute mySQL
        Case "U"
            mySQL = "UPDATE materiale SET "

            mySQL = mySQL & "mg_id=" & cmbMagazzini.ItemData(cmbMagazzini.ListIndex) & ","
            mySQL = mySQL & "sd_id=" & cmbStato_Disp.ItemData(cmbStato_Disp.ListIndex) & ","
            mySQL = mySQL & "nota_sd='" & cleanSQL(txtNotaDisp.Text) & "',"
            mySQL = mySQL & "t_id=" & cmbTipo.ItemData(cmbTipo.ListIndex) & ","
            mySQL = mySQL & "codice='" & cleanSQL(txtCodice.Text) & "',"
            mySQL = mySQL & "label='" & cleanSQL(txtLabel.Text) & "',"
            mySQL = mySQL & "marca='" & cleanSQL(txtMarca.Text) & "',"
            mySQL = mySQL & "modello='" & cleanSQL(txtModello.Text) & "',"
            mySQL = mySQL & "descrizione='" & cleanSQL(txtDescrizione.Text) & "',"
            mySQL = mySQL & "progressivo='" & cleanSQL(Val(txtProgressivo.Text)) & "',"
            mySQL = mySQL & "seriale='" & cleanSQL(txtSeriale.Text) & "',"
            mySQL = mySQL & "prezzo='" & cleanSQL(txtPrezzo.Text) & "',"
            mySQL = mySQL & "note='" & cleanSQL(txtNote.Text) & "',"
            mySQL = mySQL & "idGruppoMateriale=" & cmbCategoria.ItemData(cmbCategoria.ListIndex) & ","
            mySQL = mySQL & "del=NULL WHERE IDMateriale=" & myID
            myDB.Execute mySQL
        Case "N"
            For i = 0 To myNumInsert - 1
                If Val(txtLabelNum.Text) > 0 Then
                    myLabel = Format(Val(txtLabelNum.Text) + i, "000")
                Else
                myLabel = cleanSQL(txtLabel.Text)
                End If
                mySQL = "INSERT INTO materiale ("
                mySQL = mySQL & "mg_id,"
                mySQL = mySQL & "sd_id,"
                mySQL = mySQL & "nota_sd,"
                mySQL = mySQL & "t_id,"
                mySQL = mySQL & "codice,"
                mySQL = mySQL & "marca,"
                mySQL = mySQL & "modello,"
                mySQL = mySQL & "descrizione,"
                mySQL = mySQL & "progressivo,"
                mySQL = mySQL & "seriale,"
                mySQL = mySQL & "prezzo,"
                mySQL = mySQL & "note,"
                mySQL = mySQL & "creationDate,"
                mySQL = mySQL & "label,"
                mySQL = mySQL & "idGruppoMateriale,"
                mySQL = mySQL & "Tipo"
                
                mySQL = mySQL & " ) VALUES ( "
                
                mySQL = mySQL & "" & cmbMagazzini.ItemData(cmbMagazzini.ListIndex) & ","
                mySQL = mySQL & "" & cmbStato_Disp.ItemData(cmbStato_Disp.ListIndex) & ","
                mySQL = mySQL & "'" & cleanSQL(txtNotaDisp.Text) & "',"
                mySQL = mySQL & "" & cmbTipo.ItemData(cmbTipo.ListIndex) & ","
                mySQL = mySQL & "'" & cleanSQL(txtCodice.Text) & "',"
                mySQL = mySQL & "'" & cleanSQL(txtMarca.Text) & "',"
                mySQL = mySQL & "'" & cleanSQL(txtModello.Text) & "',"
                mySQL = mySQL & "'" & cleanSQL(txtDescrizione.Text) & "',"
                mySQL = mySQL & "'" & cleanSQL(Val(txtProgressivo.Text)) & "',"
                mySQL = mySQL & "'" & cleanSQL(txtSeriale.Text) & "',"
                mySQL = mySQL & "'" & cleanSQL(txtPrezzo.Text) & "',"
                mySQL = mySQL & "'" & cleanSQL(txtNote.Text) & "',"
                mySQL = mySQL & "'" & lastUpdate & "',"
                mySQL = mySQL & "'" & myLabel & "',"
                mySQL = mySQL & "" & cmbCategoria.ItemData(cmbCategoria.ListIndex) & ","
                mySQL = mySQL & "'')"
                myDB.Execute mySQL
                DoEvents
            Next i
    End Select

    closeMysqlDB "saveData"
    'frmMateriale.loadArticles frmMateriale.Text1
    Unload Me

End Sub
Private Function validateFields() As Boolean
    validateFields = False
'    If Trim(cleanSQL(txtCodice.Text)) = "" Then
'        MsgBox "Campo Codice obbligatorio!", vbInformation, Me.Caption
'        Exit Function
'    End If
'    If Trim(cleanSQL(txtLabel.Text)) = "" Then
'        MsgBox "Campo Label obbligatorio!", vbInformation, Me.Caption
'        Exit Function
'    End If
    If Trim(cleanSQL(txtMarca.Text)) = "" Then
        MsgBox "Campo Marca obbligatorio!", vbInformation, Me.Caption
        Exit Function
    End If
    If Trim(cleanSQL(txtModello.Text)) = "" Then
        MsgBox "Campo Modello obbligatorio!", vbInformation, Me.Caption
        Exit Function
    End If
    If Val(txtNumDulicati) > 200 Then
        MsgBox "Posso iserire/duplicare al massimo 200 articoli!", vbInformation, Me.Caption
        Exit Function
    End If
    validateFields = True
End Function

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
frmMateriale.selectedItem = searchID
End Sub

Private Sub txtNumDulicati_Change()
    If Val(txtNumDulicati) > 1 Then
        txtLabelNum.Enabled = True
        Label10.Enabled = True
    Else
        txtLabelNum.Enabled = False
        txtLabelNum.Text = ""
        Label10.Enabled = False
    End If
End Sub

Private Sub txtNumDulicati_LostFocus()
'    If Val(txtNumDulicati) > 1 Then
'        txtLabelNum.Enabled = True
'    Else
'        txtLabelNum.Enabled = False
'        txtLabelNum.Text = ""
'    End If
End Sub

