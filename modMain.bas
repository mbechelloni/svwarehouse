Attribute VB_Name = "modMain"
Public serverAddress As String
Public dbName As String
Public dbUser  As String
Public dbPassword    As String
Public printLabelMode As Integer
Public PrinterStandard As String
Public PrinterInventario As String
Public PrinterUscita As String
Public curIDMateriale As Integer
Public curCodiceAssegnazioni As String

  Public myDB As New ADODB.Connection
  Public Type codiceQuantita
    codice As Long
    quantita As Long
  End Type
Public Function openMysqlDB(myFunc As String) As Boolean

    ' TODO gestire connessione al DB
    
    Dim ps As Integer
    Dim t0 As Single
    Dim ret As Long
    
    
    'WrtMYSQLLog "open", myFunc
    t0 = Timer
    If myDB.State = 0 Then
        Do
            ret = ping(serverAddress, 2000)
            If ret >= 0 Then
                DoEvents
                If openMysqlDBSafe Then
                    openMysqlDB = True
                    Exit Function
                End If
            End If
        Loop Until Timer - t0 > 10
        openMysqlDB = False
    Else
        openMysqlDB = True
    End If
    
End Function
Public Sub Main()
    startPgr
End Sub

Public Sub startPgr()

    serverAddress = "172.22.0.2"
    dbName = "sv_data"
    dbUser = "user589Ab"
    dbPassword = "jK$2qn#P"
    PrinterStandard = GetSetting("StudioVisio", "MManager", "PrinterStandard", "")
    PrinterInventario = GetSetting("StudioVisio", "MManager", "PrinterInventario", "")
    PrinterUscita = GetSetting("StudioVisio", "MManager", "PrinterUscita", "")

    frmLavori.Show

End Sub
Public Function openMysqlDBSafe() As Boolean

    On Error GoTo hell
    
    If myDB.State = 0 Then
        myDB.Open "DRIVER={MySQL ODBC 5.2 UNICODE Driver}; SERVER=" & serverAddress & "; DATABASE=" & dbName & "; UID=" & dbUser & "; PWD=" & dbPassword & "; OPTION=3"
    End If
    openMysqlDBSafe = True
    Exit Function
    
hell:
    openMysqlDBSafe = False
        
End Function




Public Function closeMysqlDB(myFunc As String)
    
    
   ' WrtMYSQLLog "close", myFunc
    If myDB.State = 1 Then
        myDB.Close
    End If

End Function

Public Function cleanSQL(myText As String) As String

    cleanSQL = Replace(Trim(myText), "'", "''")

End Function
Public Function cleanSQLLF(myText As String) As String

    Dim tText As String
    
    tText = Replace(myText, "'", "''")
    tText = Replace(tText, vbLf, " ")
    tText = Replace(tText, vbCr, " ")
    tText = Replace(tText, vbCrLf, " ")
    cleanSQLLF = Replace(tText, "  ", " ")
          

End Function
Public Function AlphaNumericOnly(strSource As String) As String
    Dim i As Integer
    Dim strResult As String

    For i = 1 To Len(strSource)
        Select Case Asc(Mid(strSource, i, 1))
            Case 32, 45 To 57, 65 To 90, 92, 94, 95, 97 To 122: 'include 32 if you want to include space
                strResult = strResult & Mid(strSource, i, 1)
        End Select
    Next
        AlphaNumericOnly = strResult
End Function

' Search - Installed BIXOLON Printers


' Print sample label
Public Function printExitLabels(myIDMateriale As String, myEventName As String, myDestinazione As String, myDescrizione As String, myNote As String) As Boolean

    Dim bResult As Boolean

    Dim strPrinterName As String
    
    Dim MM2D As Long
    
    Dim nPaper_Width As Long
    Dim nPaper_Height As Long
    Dim nMarginX As Long
    Dim nMarginY As Long

    Dim nSpeed As Long
    Dim nDensity As Long

    Dim bAutoCut As Boolean
    Dim bReverseFeeding As Boolean

    Dim nSensorType As Long
    
    Dim Direct_MaxiCode_2DBarcode_Cmd As String
    Dim Direct_PDF_2DBarcocde_Cmd As String
    Dim QRCode_data As String
    

    strPrinterName = PrinterUscita 'cmbPrtList.Text
    If ConnectPrinter(strPrinterName) = False Then
        Exit Function
    End If
    
    ' 203 DPI : 1mm = 8 dots
    ' 300 DPI : 1mm = 12 dots
    MM2D = IIf(GetPrinterResolution() < 300, 8, 12)
    
    nPaper_Width = CInt(90 * MM2D)
    nPaper_Height = CInt(69 * MM2D)
    nMarginX = CInt(-15 * MM2D)
    nMarginY = CInt(0 * MM2D)

    nSpeed = 3
    nDensity = 14

    bReverseFeeding = False

    nSensorType = GAP
        
    '   Set the label start
    bResult = StartLabel()

    '   Set Label and Printer
    'SetConfigOfPrinter(SPEED_50, 17, TOP, false, 0, true);
    bResult = SetConfigOfPrinter(nSpeed, nDensity, TOP, bAutoCut, 0, bReverseFeeding)

    '1 Inch : 25.4mm
    '1 mm   :  8 Dot in 203 DPI such as TX400, T400, D420, D220, SRP-770, SRP-770II
    '1 mm   : 12 Dot in 300 DPI such as TX403, T403, D423, D223
    '4 Inch : 25.4  * 4 * 8 = 812.8
    '6 Inch : 25.4  * 4 * 8 = 1219.2

    'SetPaper(16, 16, 813, 1220, GAP, 0, 16); ' 4 inch (Width) * 6 inch (Hiehgt)
    bResult = SetPaper(nMarginX, nMarginY, nPaper_Width, nPaper_Height, nSensorType, 0, 16) ' 4 inch (Width) * 6 inch (Hiehgt)

    PrintDirect ("STt")

    '   Clear Buffer of Printer
    bResult = ClearBuffer()
    
    ' Print Logo
    bResult = PrintImageLib(0, 20, "logo.jpg", DITHER_1, False)

   '   Draw Lines
    bResult = PrintBlock(0, 98, 798, 100, LINE_OVER_WRITING, -1)

    bResult = PrintTrueFontLib(0, 110, "Myriad Pro Cond", 22, 0, False, True, False, myEventName)
    If myDestinazione <> "" Then
        bResult = PrintTrueFontLib(0, 110 + 54, "Myriad Pro", 10, 0, False, False, False, myDestinazione)
    End If
    If myDescrizione <> "" Then
        bResult = PrintTrueFontLib(0, 240, "Myriad Pro Cond", 36, 0, False, True, False, myDescrizione)
    End If
    If myNote <> "" Then
        bResult = PrintTrueFontLib(0, 240 + 104, "Myriad Pro Cond", 16, 0, False, False, False, myNote)
    End If

    bResult = PrintDeviceFont(658, 510, ENG_9X15, 1, 1, ROTATE_0, False, Now())

'    bResult = PrintDeviceFont(20, 174, ENG_24X38, 1, 1, ROTATE_0, False, "SHIP TO:")
'    bResult = PrintDeviceFont(20, 234, ENG_19X30, 1, 1, ROTATE_0, True, "BIXOLON")
'    bResult = PrintDeviceFont(20, 280, ENG_16X25, 1, 1, ROTATE_0, False, "7th FL, MiraeAsset Venture Tower,")
'    bResult = PrintDeviceFont(20, 310, ENG_16X25, 1, 1, ROTATE_0, False, "685, Sampyeong-dong, Bundang-gu,")
'    bResult = PrintDeviceFont(20, 340, ENG_16X25, 1, 1, ROTATE_0, False, "Seongnam-si, Gyeonggi-do,")
'    bResult = PrintDeviceFont(20, 370, ENG_16X25, 1, 1, ROTATE_0, False, "463-400, KOREA")
'
'    bResult = PrintDeviceFont(26, 421, ENG_12X20, 1, 1, ROTATE_0, False, "POSTAL CODE")
'    bResult = PrintDeviceFont(503, 798, ENG_12X20, 1, 1, ROTATE_0, False, "DESTINATION")
'    bResult = PrintDeviceFont(42, 841, ENG_32X50, 1, 1, ROTATE_0, True, "30 Kg")
'    bResult = PrintDeviceFont(25, 798, ENG_12X20, 1, 1, ROTATE_0, False, "WEIGHT:")
'    bResult = PrintDeviceFont(259, 798, ENG_12X20, 1, 1, ROTATE_0, False, "DELIVERY NO:")
'    bResult = PrintDeviceFont(23, 630, ENG_12X20, 1, 1, ROTATE_0, False, "AWB:")
'    bResult = PrintDeviceFont(274, 841, ENG_32X50, 1, 1, ROTATE_0, True, "425518")
'    bResult = PrintDeviceFont(104, 627, ENG_19X30, 1, 1, ROTATE_0, False, "8741493121")
'    bResult = PrintDeviceFont(565, 841, ENG_32X50, 1, 1, ROTATE_0, True, "ICN")
    
    '   Prints 1D Barcodes
    bResult = Print1DBarcode(30, 444, CODE39, 3, 6, 60, ROTATE_0, True, Format(myIDMateriale, "00000"))


    '   Print Command
    bResult = Prints(1, 1)

    '   Set the Label End
    EndLabel
        
    '   Disconnect Printer Driver
    bResult = DisconnectPrinter()
            
End Function

Public Function formatDate(myDate As String) As String

    
    If Len(myDate) <> 8 Then Exit Function
    formatDate = Mid(myDate, 7, 2) & "/" & Mid(myDate, 5, 2) & "/" & Mid(myDate, 1, 4)

End Function
