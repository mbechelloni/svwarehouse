VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "mscomctl.ocx"
Begin VB.Form frmEvents 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Elenco Eventi"
   ClientHeight    =   5505
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   15555
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5505
   ScaleWidth      =   15555
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   495
      Left            =   12660
      TabIndex        =   2
      Top             =   4860
      Width           =   1335
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Annulla"
      Height          =   495
      Left            =   14100
      TabIndex        =   1
      Top             =   4860
      Width           =   1335
   End
   Begin MSComctlLib.ListView lvEvents 
      Height          =   4215
      Left            =   180
      TabIndex        =   0
      Top             =   480
      Width           =   15195
      _ExtentX        =   26802
      _ExtentY        =   7435
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   4210752
      BackColor       =   16777215
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin VB.Label lblNumCommesse 
      Caption         =   "Label2"
      Height          =   435
      Left            =   240
      TabIndex        =   4
      Top             =   4800
      Width           =   2235
   End
   Begin VB.Label Label1 
      Caption         =   "Seleziona la commessa desiderata ..."
      Height          =   315
      Left            =   180
      TabIndex        =   3
      Top             =   180
      Width           =   3615
   End
End
Attribute VB_Name = "frmEvents"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdCancel_Click()

    Unload Me

End Sub

Private Sub cmdOK_Click()

    Dim mySQL As String
    Dim myRS As New ADODB.Recordset
    Dim myIDCommessa2Display As Integer
    Dim i As Integer
    
    
    For i = 1 To lvEvents.ListItems.Count
        If lvEvents.ListItems(i).Selected = True Then
            openMysqlDB ("cmdOK_Click")
            If checkCommessaInserita(lvEvents.ListItems(i).SubItems(6)) = True Then
                myIDCommessa2Display = Val(lvEvents.ListItems(i).SubItems(6))
            Else
                myIDCommessa2Display = 0
                mySQL = "INSERT INTO lavori ("
                mySQL = mySQL & "IDCommessa,"
                mySQL = mySQL & "Nome,"
                mySQL = mySQL & "Sede,"
                mySQL = mySQL & "Luogo,"
                mySQL = mySQL & "Inizio,"
                mySQL = mySQL & "Fine,"
                mySQL = mySQL & "Cliente,"
                mySQL = mySQL & "createTS "
            
                mySQL = mySQL & " ) VALUES ( "
                
                mySQL = mySQL & "" & Val(lvEvents.ListItems(i).SubItems(6)) & ","
                mySQL = mySQL & "'" & cleanSQL(lvEvents.ListItems(i).Text) & "',"
                mySQL = mySQL & "'" & cleanSQL(lvEvents.ListItems(i).SubItems(2)) & "',"
                mySQL = mySQL & "'" & cleanSQL(lvEvents.ListItems(i).SubItems(1)) & "',"
                mySQL = mySQL & "'" & Format(lvEvents.ListItems(i).SubItems(3), "YYYYMMDD") & "',"
                mySQL = mySQL & "'" & Format(lvEvents.ListItems(i).SubItems(4), "YYYYMMDD") & "',"
                mySQL = mySQL & "'" & cleanSQL(lvEvents.ListItems(i).SubItems(5)) & "',"
                mySQL = mySQL & "'" & Format(Now(), "YYYYMMDDhhmmss") & "' )"
                myDB.Execute mySQL
            End If
            closeMysqlDB ("cmdOK_Click")
            Exit For
        End If
    Next i
    frmLavori.loadLavori "", myIDCommessa2Display
    Unload Me
    
End Sub
Private Sub inserimentoCommessa()
Dim mySQL As String
Dim myRS As New ADODB.Recordset
openMysqlDB ("inserimentoCommessa")


closeMysqlDB ("inserimentoCommessa")
End Sub
Private Function checkCommessaInserita(myIDCommessa As Integer) As Boolean
Dim mySQL As String
Dim myRS As New ADODB.Recordset
checkCommessaInserita = False
    mySQL = "SELECT * FROM lavori WHERE IDCommessa = " & myIDCommessa
    myRS.CursorLocation = adUseClient
    myRS.CursorType = adOpenKeyset
    myRS.Open mySQL, myDB, adOpenForwardOnly, adLockReadOnly
    If Not myRS.EOF Then
        checkCommessaInserita = True
    End If

End Function
Private Sub Form_Load()
 
 
    Dim objSub As MSXML2.IXMLDOMNode
    Dim numCommesse As Integer
    lblNumCommesse.Caption = 0
    
    lvEvents.ListItems.Clear
    lvEvents.HideColumnHeaders = False
    lvEvents.View = lvwReport
    lvEvents.ColumnHeaders.Clear
    lvEvents.ColumnHeaders.Add , , "Nome", 5000, lvwColumnLeft
    lvEvents.ColumnHeaders.Add , , "Citta", 1500, lvwColumnLeft
    lvEvents.ColumnHeaders.Add , , "Sede", 2000, lvwColumnLeft
    lvEvents.ColumnHeaders.Add , , "Inizio", 1500, lvwColumnLeft
    lvEvents.ColumnHeaders.Add , , "Fine", 1500, lvwColumnLeft
    lvEvents.ColumnHeaders.Add , , "Cliente", 1500, lvwColumnLeft
    lvEvents.ColumnHeaders.Add , , "ID", 1500, lvwColumnLeft
    DoEvents
    
    Set xmlrequest = New MSXML2.DOMDocument60
    xmlrequest.async = False
    'xmlRequest.Load "http://www.livecongress.it/sved/edb/db_evl.php?ids=getallevents&ts=" & Format(Now(), "yyyymmddhhmmss")
    xmlrequest.Load "https://www.livecongress.it/sved/edb/db_cml.php?ids=74jrnndfhfklr40rjdsldsl0wkwksl&ts=" & Format(Now(), "yyyymmddhhmmss")
    Debug.Print xmlrequest.parseError
    If xmlrequest.parseError = 0 Then
        Debug.Print xmlrequest.xml
        
        Set objSub = xmlrequest.selectSingleNode("/commesse")
        For Each objSub In objSub.childNodes
        numCommesse = numCommesse + 1
            lvEvents.ListItems.Add , , objSub.selectSingleNode("nome").Text
            lvEvents.ListItems(lvEvents.ListItems.Count).SubItems(1) = objSub.selectSingleNode("citta").Text
            lvEvents.ListItems(lvEvents.ListItems.Count).SubItems(2) = objSub.selectSingleNode("sede").Text
            lvEvents.ListItems(lvEvents.ListItems.Count).SubItems(3) = formatDate(objSub.selectSingleNode("doIL").Text)
            lvEvents.ListItems(lvEvents.ListItems.Count).SubItems(4) = formatDate(objSub.selectSingleNode("doFL").Text)
            lvEvents.ListItems(lvEvents.ListItems.Count).SubItems(5) = objSub.selectSingleNode("cliente").Text
            lvEvents.ListItems(lvEvents.ListItems.Count).SubItems(6) = objSub.selectSingleNode("id").Text
            lvEvents.ListItems(lvEvents.ListItems.Count).Tag = objSub.selectSingleNode("id").Text
            '<IDEvent>
            '<name>
            '<shortName>
            '<startDate>
            '<endDate>
            '<venue>
            '<city>
            '<serialID>
        Next

    Else
        MsgBox "Error Downloading", vbInformation, Me.Caption
    End If
    lblNumCommesse.Caption = numCommesse
    
    
    
End Sub

Private Function formatDate(myDate As String) As String

    formatDate = Format(Mid(myDate, 7, 2) & "/" & Mid(myDate, 5, 2) & "/" & Mid(myDate, 1, 4), "dd mmm yyyy")

End Function

Private Sub lvEvents_AfterLabelEdit(Cancel As Integer, NewString As String)

    Cancel = 1
    
End Sub

Private Sub lvEvents_BeforeLabelEdit(Cancel As Integer)

    Cancel = 1
    
End Sub

Private Sub lvEvents_Click()

    If lvEvents.ListItems.Count > 0 Then
        cmdOK.Enabled = True
    End If
    
End Sub

Private Sub lvEvents_DblClick()


    cmdOK_Click

End Sub
