VERSION 5.00
Begin VB.Form frmStampa 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Preview Stampa "
   ClientHeight    =   3870
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5820
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3870
   ScaleWidth      =   5820
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Text2 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Myriad Pro Light SemiExt"
         Size            =   18
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   180
      MultiLine       =   -1  'True
      TabIndex        =   3
      Top             =   2190
      Width           =   4155
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Myriad Pro Light SemiCond"
         Size            =   24
         Charset         =   0
         Weight          =   600
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   180
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   780
      Width           =   5415
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Stampa"
      Height          =   435
      Left            =   4080
      TabIndex        =   0
      Top             =   3240
      Width           =   1275
   End
   Begin VB.Line Line3 
      X1              =   180
      X2              =   5580
      Y1              =   2760
      Y2              =   2760
   End
   Begin VB.Line Line2 
      X1              =   240
      X2              =   5640
      Y1              =   2160
      Y2              =   2160
   End
   Begin VB.Line Line1 
      X1              =   180
      X2              =   5580
      Y1              =   660
      Y2              =   660
   End
   Begin VB.Label lblBarID 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "*9129L*"
      BeginProperty Font 
         Name            =   "3 of 9 Barcode"
         Size            =   21.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   300
      TabIndex        =   1
      Top             =   3240
      Width           =   1785
   End
   Begin VB.Image Image1 
      Height          =   375
      Left            =   120
      Picture         =   "frmStampa.frx":0000
      Stretch         =   -1  'True
      Top             =   180
      Width           =   1500
   End
End
Attribute VB_Name = "frmStampa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public myPrintList As String
Private Sub Command1_Click()

    Command1.Visible = fase
    Me.PrintForm
    Command1.Visible = True
    
    
End Sub

Public Sub PrintLabels()
    
    Dim myRs As New ADODB.Recordset
    Dim mySQL As String
    Dim params() As String
    Dim myNomeBreve As String
    Dim i As Integer
    params = Split(myPrintList, ",")
    For i = 0 To UBound(params())
        mySQL = "SELECT nomeBreve,noteAssegnazione,dettaglioAssegnazione,pc_Elenco_assegnazioni.descrizione as DesAss,IDEvento,Abbreviazione,NomeBreve,IDArticolo,NomeEvento,numeroriassegnato,caratterecontrollo,marcamodello FROM PC_Articoli_Eventi LEFT JOIN preventivi ON PC_Articoli_Eventi.IDEvento = preventivi.IDPreventivo LEFT JOIN dettaglioarticoli ON PC_Articoli_Eventi.IDArticolo = dettaglioarticoli.IDComponente LEFT JOIN PC_Elenco_Assegnazioni ON PC_Articoli_Eventi.IDAssegnazione = pc_Elenco_assegnazioni.IDAssegnazione where ID =" & Val(params(i))
        myRs.Open mySQL, cnn, adOpenKeyset, adLockReadOnly
        If Not myRs.EOF Then
        Me.lblEvento = "" & myRs("IDEvento") & " - " & IIf("" & myRs("NomeBreve") = "", "" & myRs("NomeEvento"), "" & myRs("NomeBreve"))
        Me.lblAssegnazione = "" & myRs("DesAss")
        Me.lblAssegnazione2 = "" & myRs("dettaglioAssegnazione")
        Me.lblAssegnazione3 = "" & myRs("Abbreviazione")
        Me.lblbar = "" ' "*PM" & Val(params(i)) & "*"
        Me.lbldata = "" & Format(Now, "dd mmm yyyy - hh:mm:ss")
        Me.lblBarID = "*" & myRs("IDArticolo") & myRs("caratterecontrollo") & "*"
        Me.lblIDArticolo = "" & myRs("IDArticolo")
        Me.lblNumero = "" & myRs("numeroriassegnato")
        
        Label11.Caption = "" & myRs("noteAssegnazione")
        Image1.Visible = Label11.Caption <> ""
    
            If "" & myRs("Abbreviazione") = "" Then
                lblAssegnazione.Left = 2985
                lblAssegnazione2.Left = 2985
                lblbar.Left = 2985
            Else
                lblAssegnazione.Left = 2205
                lblAssegnazione2.Left = 2205
                lblbar.Left = 2205
            End If
        End If
        myRs.Close
        Me.Show
      ' Exit Sub
        Me.PrintForm
        'delay 0.5
    Next i
    Unload Me
    
End Sub

Private Sub Label5_Click()

End Sub

