VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMateriale 
   Caption         =   "Materiale"
   ClientHeight    =   11055
   ClientLeft      =   60
   ClientTop       =   405
   ClientWidth     =   20475
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   11055
   ScaleWidth      =   20475
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame frmPrintLabels 
      Caption         =   "Stampa etichette"
      Height          =   2655
      Left            =   120
      TabIndex        =   13
      Top             =   8280
      Width           =   10215
      Begin VB.CommandButton cmdPrintLabels 
         Caption         =   "Stampa"
         Height          =   555
         Left            =   8640
         TabIndex        =   29
         Top             =   840
         Width           =   1455
      End
      Begin VB.OptionButton optLabelsOption 
         Caption         =   "Barcode"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   28
         Top             =   300
         Value           =   -1  'True
         Width           =   1875
      End
      Begin VB.OptionButton optLabelsOption 
         Caption         =   "Barcode x 2"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   27
         Top             =   660
         Width           =   1815
      End
      Begin VB.OptionButton optLabelsOption 
         Caption         =   "Barcode + Testo"
         Height          =   255
         Index           =   2
         Left            =   2160
         TabIndex        =   26
         Top             =   360
         Width           =   1875
      End
      Begin VB.OptionButton optLabelsOption 
         Caption         =   "Testo Libero"
         Height          =   255
         Index           =   4
         Left            =   2160
         TabIndex        =   25
         Top             =   660
         Width           =   1815
      End
      Begin VB.OptionButton optLabelsOption 
         Caption         =   "Barcode + Accessori"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   24
         Top             =   1020
         Width           =   1875
      End
      Begin VB.Frame frmSX 
         Caption         =   "SX"
         Height          =   1155
         Left            =   4080
         TabIndex        =   21
         Top             =   300
         Width           =   2100
         Begin VB.TextBox txtLabelSX 
            Height          =   315
            Left            =   120
            TabIndex        =   23
            Top             =   660
            Width           =   1800
         End
         Begin VB.TextBox txtCodeSX 
            Height          =   315
            Left            =   120
            TabIndex        =   22
            Top             =   240
            Width           =   1800
         End
      End
      Begin VB.Frame frmDX 
         Caption         =   "DX"
         Height          =   1155
         Left            =   6240
         TabIndex        =   18
         Top             =   300
         Width           =   2100
         Begin VB.TextBox txtCodeDX 
            Height          =   315
            Left            =   120
            TabIndex        =   20
            Top             =   240
            Width           =   1800
         End
         Begin VB.TextBox txtLabelDX 
            Height          =   315
            Left            =   120
            TabIndex        =   19
            Top             =   660
            Width           =   1800
         End
      End
      Begin VB.OptionButton optLabelsOption 
         Caption         =   "Etichetta compatta"
         Height          =   255
         Index           =   5
         Left            =   2160
         TabIndex        =   17
         Top             =   1020
         Width           =   1815
      End
      Begin VB.OptionButton optLabelsOption 
         Caption         =   "Etichetta PVC Bianca"
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   16
         Top             =   1380
         Width           =   1815
      End
      Begin VB.OptionButton optLabelsOption 
         Caption         =   "Et. numerica da-a"
         Height          =   255
         Index           =   7
         Left            =   2160
         TabIndex        =   15
         Top             =   1380
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "Label1"
         Height          =   435
         Left            =   8760
         TabIndex        =   30
         Top             =   240
         Width           =   1395
      End
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Crea Progressivi"
      Height          =   615
      Left            =   15120
      TabIndex        =   12
      Top             =   8400
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Frame Frame2 
      Caption         =   "Filtri di ricerca"
      Height          =   1095
      Left            =   3600
      TabIndex        =   11
      Top             =   60
      Width           =   12975
      Begin VB.CheckBox chkCancellati 
         Caption         =   "Mostra cancellati"
         Height          =   255
         Left            =   11220
         TabIndex        =   44
         Top             =   540
         Width           =   1695
      End
      Begin VB.ComboBox cmbFiltroTipo 
         Height          =   315
         Left            =   240
         TabIndex        =   35
         Text            =   "Filtro tipo"
         Top             =   540
         Width           =   2595
      End
      Begin VB.ComboBox cmbFiltroMagazzini 
         Height          =   315
         Left            =   5820
         TabIndex        =   34
         Text            =   "Filtro Magazzini"
         Top             =   540
         Width           =   2595
      End
      Begin VB.ComboBox cmbFiltroCategoria 
         Height          =   315
         Left            =   3060
         TabIndex        =   32
         Text            =   "Filtro Categoria"
         Top             =   540
         Width           =   2595
      End
      Begin VB.Label Label7 
         Caption         =   "Magazzino di appartenenza"
         Height          =   255
         Left            =   5880
         TabIndex        =   43
         Top             =   300
         Width           =   2115
      End
      Begin VB.Label Label3 
         Caption         =   "Categoria"
         Height          =   255
         Left            =   3060
         TabIndex        =   33
         Top             =   300
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Tipo di articoli"
         Height          =   255
         Left            =   240
         TabIndex        =   31
         Top             =   300
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Assegnazioni multiple"
      Height          =   2655
      Left            =   10440
      TabIndex        =   8
      Top             =   8280
      Width           =   4515
      Begin VB.ComboBox cmbTipo 
         Height          =   315
         Left            =   180
         TabIndex        =   41
         Text            =   "Tipo"
         Top             =   1740
         Width           =   2775
      End
      Begin VB.CommandButton cmdUpdTipo 
         Caption         =   "Assegna"
         Height          =   315
         Left            =   3120
         TabIndex        =   40
         Top             =   1740
         Width           =   1095
      End
      Begin VB.ComboBox cmbMagazzini 
         Height          =   315
         Left            =   180
         TabIndex        =   38
         Text            =   "Magazzino"
         Top             =   1140
         Width           =   2775
      End
      Begin VB.CommandButton cmdUpdMagazzini 
         Caption         =   "Assegna"
         Height          =   315
         Left            =   3120
         TabIndex        =   37
         Top             =   1140
         Width           =   1095
      End
      Begin VB.CommandButton cmdUpdateGruppi 
         Caption         =   "Assegna"
         Height          =   315
         Left            =   3120
         TabIndex        =   10
         Top             =   540
         Width           =   1095
      End
      Begin VB.ComboBox cmbCategoria 
         Height          =   315
         Left            =   180
         TabIndex        =   9
         Text            =   "Categoria"
         Top             =   540
         Width           =   2775
      End
      Begin VB.Label Label6 
         Caption         =   "Tipologia di articolo"
         Height          =   255
         Left            =   180
         TabIndex        =   42
         Top             =   1500
         Width           =   1815
      End
      Begin VB.Label Label5 
         Caption         =   "Magazzino di appartenenza"
         Height          =   255
         Left            =   180
         TabIndex        =   39
         Top             =   900
         Width           =   1815
      End
      Begin VB.Label Label4 
         Caption         =   "Categoria di appartenenza"
         Height          =   255
         Left            =   180
         TabIndex        =   36
         Top             =   300
         Width           =   1815
      End
   End
   Begin VB.CommandButton cmdNuovo 
      Caption         =   "Nuovo"
      Height          =   1215
      Left            =   18240
      TabIndex        =   4
      Top             =   8400
      Width           =   2055
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Cerca"
      Height          =   315
      Left            =   19500
      TabIndex        =   1
      Top             =   180
      Width           =   735
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Left            =   16800
      TabIndex        =   0
      Top             =   180
      Width           =   2415
   End
   Begin MSComctlLib.ListView lvArticoli 
      Height          =   6915
      Left            =   120
      TabIndex        =   14
      Top             =   1260
      Width           =   20115
      _ExtentX        =   35481
      _ExtentY        =   12197
      View            =   3
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   4210752
      BackColor       =   16777215
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin VB.Label lblCode1 
      AutoSize        =   -1  'True
      Caption         =   "Label Code"
      BeginProperty Font 
         Name            =   "Myriad Pro Cond"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   14160
      TabIndex        =   7
      Top             =   4680
      Width           =   990
   End
   Begin VB.Label lblTest1 
      AutoSize        =   -1  'True
      Caption         =   "Label4"
      BeginProperty Font 
         Name            =   "Myriad Pro Cond"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   14460
      TabIndex        =   6
      Top             =   4440
      Width           =   495
   End
   Begin VB.Label lblCode 
      AutoSize        =   -1  'True
      Caption         =   "Label Code"
      BeginProperty Font 
         Name            =   "Myriad Pro Cond"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   13200
      TabIndex        =   5
      Top             =   4680
      Width           =   990
   End
   Begin VB.Label lblTest 
      AutoSize        =   -1  'True
      Caption         =   "Label4"
      BeginProperty Font 
         Name            =   "Myriad Pro Cond"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   13440
      TabIndex        =   3
      Top             =   4260
      Width           =   495
   End
   Begin VB.Label lblArtCount 
      Height          =   915
      Left            =   120
      TabIndex        =   2
      Top             =   180
      Width           =   3255
   End
End
Attribute VB_Name = "frmMateriale"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private cmbLoading As Boolean
Public selectedItem As Integer

Public Sub loadArticles(search As String)


    Dim myRS As New ADODB.Recordset
    Dim mySQL As String
    Dim prezzoTotale As Single
    Dim printedLabels As Integer
    Dim myIsNullCondition As String
    Dim myFilterCondition As String
    Dim myConditions As String
    openMysqlDB ("ReadArticles")
    

    myRS.CursorLocation = adUseClient
    myRS.CursorType = adOpenKeyset
    
    If chkCancellati = 0 Then myConditions = " AND del IS NULL"
    If cmbFiltroCategoria.ItemData(cmbFiltroCategoria.ListIndex) > 0 Then myConditions = myConditions & " AND m.idGruppoMateriale =" & cmbFiltroCategoria.ItemData(cmbFiltroCategoria.ListIndex)
    If cmbFiltroMagazzini.ItemData(cmbFiltroMagazzini.ListIndex) > 0 Then myConditions = myConditions & " AND m.mg_id =" & cmbFiltroMagazzini.ItemData(cmbFiltroMagazzini.ListIndex)
    If cmbFiltroTipo.ItemData(cmbFiltroTipo.ListIndex) > 0 Then myConditions = myConditions & " AND m.t_id =" & cmbFiltroTipo.ItemData(cmbFiltroTipo.ListIndex)

'    If chkStandard.Value = 1 And chkGenerico.Value = 1 And chkKit.Value = 1 Then 'tutti
    
'    ElseIf chkStandard.Value = 1 And chkGenerico.Value = 1 And chkKit.Value = 0 Then 'quelli standard e quelli generici
'        myConditions = myConditions & " AND kit = 0"
'    ElseIf chkStandard.Value = 1 And chkGenerico.Value = 0 And chkKit.Value = 0 Then 'quelli standard soli
'        myConditions = myConditions & " AND (numero = 0 AND kit = 0)"
'    ElseIf chkStandard.Value = 0 And chkGenerico.Value = 0 And chkKit.Value = 1 Then 'quelli kit soli
'        myConditions = myConditions & " AND kit = 1"
'    ElseIf chkStandard.Value = 1 And chkGenerico.Value = 0 And chkKit.Value = 1 Then 'quelli kit + quelli standard
'        myConditions = myConditions & " AND numero = 0 AND kit = 1"
'    ElseIf chkStandard.Value = 0 And chkGenerico.Value = 0 And chkKit.Value = 0 Then 'nessuno
'        Exit Sub
'    End If
    
'    If chkStandard.Value = 1 Then myConditions = myConditions & " AND numero = 0"
'    If chkGenerico.Value = 1 Then myConditions = myConditions & " AND numero > 0"
'    If chkKit.Value = 1 Then myConditions = myConditions & " AND numero > 0"
    mySQL = "SELECT m.progressivo,m.tipo,m.idmateriale,m.codice,m.label,m.marca,m.modello,m.descrizione as desm,m.note as notm,g.descrizione as desg,m.prezzo,m.del,m.printlastlabel,g.idGruppoMateriale,mg.mg_nome,sd.sd_stato,t.t_codice "
    mySQL = mySQL & "FROM materiale as m "
    mySQL = mySQL & "INNER JOIN gruppiMateriale as g ON m.idGruppoMateriale = g.idGruppoMateriale "
    mySQL = mySQL & "LEFT JOIN magazzini as mg ON m.mg_id = mg.id_mg "
    mySQL = mySQL & "LEFT JOIN stato_disp as sd ON m.sd_id = sd.id_sd "
    mySQL = mySQL & "LEFT JOIN tipo as t ON m.t_id = t.id_t "
    Select Case search
    Case "prezzo0"
        'mySQL = "SELECT m.progressivo,m.tipo,m.idmateriale,m.codice,m.label,m.marca,m.modello,m.descrizione as desm,m.note as notm,g.descrizione as desg,m.prezzo,m.del,m.printlastlabel,g.idGruppoMateriale  FROM materiale as m INNER JOIN gruppiMateriale as g ON m.idGruppoMateriale = g.idGruppoMateriale WHERE prezzo = 0" & myConditions & " ORDER  BY IDMateriale ASC"
        mySQL = mySQL & "WHERE prezzo = 0" & myConditions & " ORDER  BY IDMateriale ASC"
    Case ""
        'mySQL = "SELECT m.progressivo,m.tipo,m.idmateriale,m.codice,m.label,m.marca,m.modello,m.descrizione as desm,m.note as notm,g.descrizione as desg,m.prezzo,m.del,m.printlastlabel,g.idGruppoMateriale  FROM materiale as m INNER JOIN gruppiMateriale as g ON m.idGruppoMateriale = g.idGruppoMateriale WHERE 1=1" & myConditions & " ORDER BY IDMateriale ASC"
        mySQL = mySQL & "WHERE 1=1" & myConditions & " ORDER BY IDMateriale ASC"
    Case Else
         If IsNumeric(search) And InStr(search, ".") = 0 Then
            'mySQL = "SELECT m.progressivo,m.tipo,m.idmateriale,m.codice,m.label,m.marca,m.modello,m.descrizione as desm,m.note as notm,g.descrizione as desg,m.prezzo,m.del,m.printlastlabel,g.idGruppoMateriale FROM materiale as m INNER JOIN gruppiMateriale as g ON m.idGruppoMateriale = g.idGruppoMateriale WHERE IDMateriale = " & search & "" & myConditions & " ORDER  BY IDMateriale ASC"
            mySQL = mySQL & "WHERE IDMateriale = " & search & "" & myConditions & " ORDER  BY IDMateriale ASC"
         Else
             'mySQL = "SELECT m.progressivo,m.tipo,m.idmateriale,m.codice,m.label,m.marca,m.modello,m.descrizione as desm,m.note as notm,g.descrizione as desg,m.prezzo,m.del,m.printlastlabel,g.idGruppoMateriale  FROM materiale as m INNER JOIN gruppiMateriale as g ON m.idGruppoMateriale = g.idGruppoMateriale WHERE (m.label LIKE '%" & search & "%' OR m.codice LIKE '%" & search & "%' OR m.marca LIKE '%" & search & "%' OR m.modello LIKE '%" & search & "%'  OR m.seriale LIKE '%" & search & "%'  OR m.descrizione LIKE '%" & search & "%'  OR m.prezzo LIKE '%" & search & "%' OR m.note LIKE '%" & search & "%')" & myConditions & " ORDER BY IDMateriale ASC"
             mySQL = mySQL & "WHERE (m.label LIKE '%" & search & "%' OR m.codice LIKE '%" & search & "%' OR m.marca LIKE '%" & search & "%' OR m.modello LIKE '%" & search & "%'  OR m.seriale LIKE '%" & search & "%'  OR m.descrizione LIKE '%" & search & "%'  OR m.prezzo LIKE '%" & search & "%' OR m.note LIKE '%" & search & "%')" & myConditions & " ORDER BY IDMateriale ASC"
         End If
    End Select
    lvArticoli.ListItems.Clear
    lvArticoli.Sorted = False
    myRS.Open mySQL, myDB, adOpenForwardOnly, adLockReadOnly
    While Not myRS.EOF
        lvArticoli.ListItems.Add , "A" & myRS("IDMateriale"), Format(myRS("IDMateriale"), "00000")
        lvArticoli.ListItems(lvArticoli.ListItems.Count).SubItems(1) = "" & myRS("codice")
        lvArticoli.ListItems(lvArticoli.ListItems.Count).SubItems(2) = "" & myRS("label")
        lvArticoli.ListItems(lvArticoli.ListItems.Count).SubItems(3) = "" & myRS("desg")
        lvArticoli.ListItems(lvArticoli.ListItems.Count).SubItems(4) = "" & myRS("marca")
        lvArticoli.ListItems(lvArticoli.ListItems.Count).SubItems(5) = "" & myRS("modello")
        lvArticoli.ListItems(lvArticoli.ListItems.Count).SubItems(6) = "" & myRS("notm")
        lvArticoli.ListItems(lvArticoli.ListItems.Count).SubItems(7) = Replace("" & myRS("prezzo"), ",", ".")
        lvArticoli.ListItems(lvArticoli.ListItems.Count).SubItems(8) = IIf("" & myRS("del") <> "", "X", "")
        lvArticoli.ListItems(lvArticoli.ListItems.Count).SubItems(9) = "" & myRS("printLastLabel")
        lvArticoli.ListItems(lvArticoli.ListItems.Count).SubItems(10) = "" & myRS("t_codice")
        lvArticoli.ListItems(lvArticoli.ListItems.Count).SubItems(11) = "" & myRS("progressivo")
        prezzoTotale = prezzoTotale + CSng(myRS("prezzo"))
        printedLabels = printedLabels + IIf("" & myRS("printLastLabel") <> "", 1, 0)
        myRS.MoveNext
    Wend
    lblArtCount.Caption = lvArticoli.ListItems.Count & "/" & printedLabels & " Articoli/Etichette - Prezzo tot " & prezzoTotale
    
    closeMysqlDB ("ReadArticles")
 

End Sub

Private Function clearSQL(mySQL As String) As String

    clearSQL = Trim(mySQL)
    clearSQL = Replace(clearSQL, "'", "''")
    
End Function


Private Sub chkCancellati_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    loadArticles Text1.Text
End Sub


Private Sub cmbFiltroCategoria_Click()
If cmbLoading Then Exit Sub
    loadArticles Text1.Text

End Sub

Private Sub cmbFiltroMagazzini_Click()
If cmbLoading Then Exit Sub
    loadArticles Text1.Text
End Sub



Private Sub cmbFiltroTipo_Click()
If cmbLoading Then Exit Sub
    loadArticles Text1.Text
End Sub

Private Sub cmdNuovo_Click()
    frmEdit.searchID = 0
    frmEdit.Show 1
End Sub

Private Sub cmdUpdateGruppi_Click()
    Dim i As Integer
    Dim myRS As New ADODB.Recordset
    Dim mySQL As String
    Dim ret As Integer
    
    On Error GoTo hell
    ret = MsgBox("Vuoi assegnare la categoria a tutti i record selezionati?", vbQuestion + vbYesNo, Me.Caption)
    If ret = vbNo Then Exit Sub
    
    openMysqlDB ("cmdUpdateGruppi_Click")
    For i = 1 To Me.lvArticoli.ListItems.Count
       If lvArticoli.ListItems(i).Selected = True Then
        mySQL = "UPDATE materiale SET idGruppoMateriale=" & cmbCategoria.ItemData(cmbCategoria.ListIndex) & " WHERE IDMateriale = " & Val(lvArticoli.ListItems(i).Text)
        myDB.Execute mySQL
       End If
    Next i
    closeMysqlDB ("cmdUpdateGruppi_Click")
    Command1_Click
    Exit Sub
hell:
End Sub

Private Sub cmdUpdMagazzini_Click()
    Dim i As Integer
    Dim myRS As New ADODB.Recordset
    Dim mySQL As String
    Dim ret As Integer
    
    On Error GoTo hell
    ret = MsgBox("Vuoi assegnare tutti gli articoli selezionati al magazzino '" & cmbMagazzini.List(cmbMagazzini.ListIndex) & "'?", vbQuestion + vbYesNo, Me.Caption)
    If ret = vbNo Then Exit Sub
    
    openMysqlDB ("cmdUpdMagazzini_Click")
    For i = 1 To Me.lvArticoli.ListItems.Count
       If lvArticoli.ListItems(i).Selected = True Then
        mySQL = "UPDATE materiale SET mg_id=" & cmbMagazzini.ItemData(cmbMagazzini.ListIndex) & " WHERE IDMateriale = " & Val(lvArticoli.ListItems(i).Text)
        myDB.Execute mySQL
       End If
    Next i
    closeMysqlDB ("cmdUpdMagazzini_Click")
    Command1_Click
    Exit Sub
hell:
End Sub

Private Sub cmdUpdTipo_Click()
    Dim i As Integer
    Dim myRS As New ADODB.Recordset
    Dim mySQL As String
    Dim ret As Integer
    
    On Error GoTo hell
    ret = MsgBox("Vuoi assegnare tutti gli articoli selezionati alla tipologia '" & cmbTipo.List(cmbTipo.ListIndex) & "'?", vbQuestion + vbYesNo, Me.Caption)
    If ret = vbNo Then Exit Sub
    
    openMysqlDB ("cmdUpdTipo_Click")
    For i = 1 To Me.lvArticoli.ListItems.Count
       If lvArticoli.ListItems(i).Selected = True Then
        mySQL = "UPDATE materiale SET t_id=" & cmbTipo.ItemData(cmbTipo.ListIndex) & " WHERE IDMateriale = " & Val(lvArticoli.ListItems(i).Text)
        myDB.Execute mySQL
       End If
    Next i
    closeMysqlDB ("cmdUpdTipo_Click")
    Command1_Click
    Exit Sub
hell:
End Sub

Private Sub Command1_Click()


    loadArticles Text1.Text

End Sub

Private Sub printLabels(code1 As String, Label1 As String, code2 As String, Label2 As String, myPrintType As Integer)


 Dim bResult As Boolean

    Dim strPrinterName As String
    
    Dim MM2D As Long
    
    Dim nPaper_Width As Long
    Dim nPaper_Height As Long
    Dim nMarginX As Long
    Dim nMarginY As Long

    Dim nSpeed As Long
    Dim nDensity As Long

    Dim bAutoCut As Boolean
    Dim bReverseFeeding As Boolean

    Dim nSensorType As Long
    
    Dim Direct_MaxiCode_2DBarcode_Cmd As String
    Dim Direct_PDF_2DBarcocde_Cmd As String
    Dim QRCode_data As String
    Dim w As Single
    Dim w1 As Single
    Dim wn As Single
    Dim wn1 As Single
    Dim nv As Single
    Dim nv1 As Single
    Dim hgap As Single
    Dim i As Integer
    Dim myR1Y As Integer
    Dim myR2Y As Integer
        strPrinterName = PrinterInventario 'frmMain.cmbPrtList.Text
        If ConnectPrinter(strPrinterName) = False Then
            Exit Sub
        End If
        
        ' 203 DPI : 1mm = 8 dots
        ' 300 DPI : 1mm = 12 dots
        MM2D = IIf(GetPrinterResolution() < 300, 8, 12)
        
        nPaper_Width = CInt(73 * MM2D)
        nPaper_Height = CInt(18 * MM2D)
        nMarginX = CInt(0 * MM2D)
        nMarginY = CInt(0 * MM2D)
    
        nSpeed = 0
        nDensity = 14
    
        bReverseFeeding = 0
    
        nSensorType = GAP
            
        '   Set the label start
        bResult = StartLabel()
    
        '   Set Label and Printer
        'SetConfigOfPrinter(SPEED_50, 17, TOP, false, 0, true);
        bResult = SetConfigOfPrinter(nSpeed, nDensity, TOP, bAutoCut, 0, bReverseFeeding)
    
        '1 Inch : 25.4mm
        '1 mm   :  8 Dot in 203 DPI such as TX400, T400, D420, D220, SRP-770, SRP-770II
        '1 mm   : 12 Dot in 300 DPI such as TX403, T403, D423, D223
        '4 Inch : 25.4  * 4 * 8 = 812.8
        '6 Inch : 25.4  * 4 * 8 = 1219.2
    
        'SetPaper(16, 16, 813, 1220, GAP, 0, 16); ' 4 inch (Width) * 6 inch (Hiehgt)
        bResult = SetPaper(nMarginX, nMarginY, nPaper_Width, nPaper_Height, nSensorType, 0, 16) ' 4 inch (Width) * 6 inch (Hiehgt)
        PrintDirect ("STt")
        '   Clear Buffer of Printer
        bResult = ClearBuffer()
        lblTest.Caption = Trim(Label1)
        lblCode.Caption = Trim(code1)
        w = (lblTest.Width / 15)
        wn = (w - 4) / (87 - 4)
        nv = (16) - (16 - 4.5) * wn
        w1 = (lblCode.Width / 15)
        wn1 = (w1 - 4) / (87 - 4)
        nv1 = (16) - (16 - 4.5) * wn1
        hgap = 38 * 8
        lblTest1.Caption = Trim(Label2)
        lblCode1.Caption = Trim(code2)
        w = (lblTest.Width / 15)
        wn = (w - 4) / (87 - 4)
        nv = (16) - (16 - 4.5) * wn
        w1 = (lblCode.Width / 15)
        wn1 = (w1 - 4) / (87 - 4)
        nv1 = (16) - (16 - 4.5) * wn1
        'Y riga 1
        myR1Y = 65 + 5
        'Y riga 2
        myR2Y = 105 + 5
        Select Case myPrintType
        Case 0, 1 'ho sempre un barcode
            bResult = PrintTrueFontLib(autoSizeText(lblTest, Trim(Label1)) * 8, myR2Y, "Myriad Pro Cond", 12, 0, False, True, False, Trim(Label1))
            bResult = Print1DBarcode(2.5 * 8, myR1Y, CODE128, 3, 6, 45, ROTATE_0, False, Format(code1, "00000"))
             If code2 <> "" Then 'solo qui posso avere una etichetta singola (caso 0 numero dispari di etichette)
                bResult = PrintTrueFontLib(hgap + autoSizeText(lblTest, Trim(Label2)) * 8, myR2Y, "Myriad Pro Cond", 12, 0, False, True, False, Trim(Label2))
                bResult = Print1DBarcode(hgap + 2.5 * 8, myR1Y, CODE128, 3, 6, 45, ROTATE_0, False, Format(code2, "00000"))
             End If
        Case 2, 3 'ho un barcode a SX e testo a DX
            'SX
            bResult = PrintTrueFontLib(autoSizeText(lblTest, Trim(Label1)) * 8, myR2Y, "Myriad Pro Cond", 12, 0, False, True, False, Trim(Label1))
            bResult = Print1DBarcode(2.5 * 8, myR1Y, CODE128, 3, 6, 45, ROTATE_0, False, Format(code1, "00000"))
            'DX
            If Trim(Label2) <> "" Then
                bResult = PrintTrueFontLib(hgap + autoSizeText(lblTest, Trim(Label2)) * 8, myR2Y, "Myriad Pro Cond", 12, 0, False, True, False, Trim(Label2))
            End If
            If Trim(code2) <> "" Then
                bResult = PrintTrueFontLib(hgap + autoSizeText(lblTest, Trim(code2)) * 8, myR1Y, "Myriad Pro Cond", 12, 0, False, True, False, Trim(code2))
            End If
        Case 4 ' ho testo a SX e DX
            'SX
            If Trim(Label1) <> "" Then
                bResult = PrintTrueFontLib(autoSizeText(lblTest, Trim(Label1)) * 8, myR2Y, "Myriad Pro Cond", 12, 0, False, True, False, Trim(Label1))
            End If
            If Trim(code1) <> "" Then
                bResult = PrintTrueFontLib(autoSizeText(lblTest, Trim(code1)) * 8, myR1Y, "Myriad Pro Cond", 12, 0, False, True, False, Trim(code1))
            End If
            'DX
            If Trim(Label2) <> "" Then
                bResult = PrintTrueFontLib(hgap + autoSizeText(lblTest, Trim(Label2)) * 8, myR2Y, "Myriad Pro Cond", 12, 0, False, True, False, Trim(Label2))
            End If
            If Trim(code2) <> "" Then
                bResult = PrintTrueFontLib(hgap + autoSizeText(lblTest, Trim(code2)) * 8, myR1Y, "Myriad Pro Cond", 12, 0, False, True, False, Trim(code2))
            End If
        Case 5 ' ho testo compatto a SX e DX
            'SX
            If Trim(Label1) <> "" Then
                bResult = PrintTrueFontLib(autoSizeText(lblTest, Trim(Label1)) * 8 - 10, myR2Y - 15, "Myriad Pro Cond", 12, 0, False, True, False, Trim(Label1))
            End If
            If Trim(code1) <> "" Then
                bResult = PrintTrueFontLib(autoSizeText(lblTest, Trim(code1)) * 8, myR1Y, "Myriad Pro Cond", 10, 0, False, True, False, Trim(code1))
            End If
            'DX
            If Trim(Label2) <> "" Then
                bResult = PrintTrueFontLib(hgap + autoSizeText(lblTest, Trim(Label2)) * 8 - 10, myR2Y - 15, "Myriad Pro Cond", 12, 0, False, True, False, Trim(Label2))
            End If
            If Trim(code2) <> "" Then
                bResult = PrintTrueFontLib(hgap + autoSizeText(lblTest, Trim(code2)) * 8, myR1Y, "Myriad Pro Cond", 10, 0, False, True, False, Trim(code2))
            End If
        Case 7 'ho copppia di testi singoli centrati e grandi
            'SX
            If Trim(Label1) <> "" Then
                bResult = PrintTrueFontLib(autoSizeText(lblTest, Trim(Label1)) * 8, 80, "Myriad Pro Cond", 20, 0, False, True, False, Trim(Label1))
            End If

            'DX
            If Trim(Label2) <> "" Then
                bResult = PrintTrueFontLib(hgap + autoSizeText(lblTest, Trim(Label2)) * 8, 80, "Myriad Pro Cond", 20, 0, False, True, False, Trim(Label2))
            End If

        End Select
        
        
        
'        If myPrintType < 1 Then 'ho sempre un barcode a SX
'            bResult = PrintTrueFontLib(nv * 8, 105, "Myriad Pro Cond", 12, 0, False, True, False, lblTest.Caption)
'            bResult = Print1DBarcode(2.5 * 8, 65, CODE128, 3, 6, 45, ROTATE_0, False, Format(code1, "00000"))
'        Else 'ho un txt a sin
'            bResult = PrintTrueFontLib(nv * 8, 100, "Myriad Pro Cond", 12, 0, False, True, False, lblTest.Caption)
'            bResult = PrintTrueFontLib(nv1 * 8, 65, "Myriad Pro Cond", 12, 0, False, True, False, lblCode.Caption)
'        End If
'        If code2 <> "" Then
'
'            If myPrintType < 0 Then 'ho un barcode a dx
'                bResult = PrintTrueFontLib(hgap + nv * 8, 105, "Myriad Pro Cond", 12, 0, False, True, False, lblTest.Caption)
'                bResult = Print1DBarcode(hgap + 2.5 * 8, 65, CODE128, 3, 6, 45, ROTATE_0, False, Format(code1, "00000"))
'            Else 'ho un txt a destra
'                bResult = PrintTrueFontLib(hgap + nv * 8, 100, "Myriad Pro Cond", 12, 0, False, True, False, lblTest.Caption)
'                bResult = PrintTrueFontLib(hgap + nv1 * 8, 65, "Myriad Pro Cond", 12, 0, False, True, False, lblCode.Caption)
'            End If
'        End If
        '   Print Command
        bResult = Prints(1, 1)
        '   Set the Label End
        EndLabel
        '   Disconnect Printer Driver
        bResult = DisconnectPrinter()
  
    
    


End Sub


Private Sub printPVC10x10Label(code As String, row1 As String, row2 As String, prog As Integer)


    Dim bResult As Boolean

    Dim strPrinterName As String
    
    Dim MM2D As Long
    
    Dim nPaper_Width As Long
    Dim nPaper_Height As Long
    Dim nMarginX As Long
    Dim nMarginY As Long

    Dim nSpeed As Long
    Dim nDensity As Long

    Dim bAutoCut As Boolean
    Dim bReverseFeeding As Boolean

    Dim nSensorType As Long
    
    Dim Direct_MaxiCode_2DBarcode_Cmd As String
    Dim Direct_PDF_2DBarcocde_Cmd As String
    Dim QRCode_data As String
    Dim w As Single
    Dim w1 As Single
    Dim wn As Single
    Dim wn1 As Single
    Dim nv As Single
    Dim nv1 As Single
    Dim hgap As Single
    Dim i As Integer
    Dim myR1Y As Integer
    Dim myR2Y As Integer
    
        strPrinterName = PrinterInventario 'frmMain.cmbPrtList.Text
        If ConnectPrinter(strPrinterName) = False Then
            Exit Sub
        End If
        
        ' 203 DPI : 1mm = 8 dots
        ' 300 DPI : 1mm = 12 dots
        MM2D = IIf(GetPrinterResolution() < 300, 8, 12)
        
        
        ' Dimensione Etichetta 85x85 mm
        nPaper_Width = CInt(85 * MM2D)
        nPaper_Height = CInt(85 * MM2D)
        
        nMarginX = CInt(0 * MM2D)
        nMarginY = CInt(0 * MM2D)
'
        nSpeed = 0
        nDensity = 20
'
        bReverseFeeding = 0
'
        nSensorType = GAP
'
'        '   Set the label start
        bResult = StartLabel()
'
        '   Set Label and Printer
        'SetConfigOfPrinter(SPEED_50, 17, TOP, false, 0, true);
        bResult = SetConfigOfPrinter(nSpeed, nDensity, BOTTOM, bAutoCut, 0, False)
'
'        '1 Inch : 25.4mm
'        '1 mm   :  8 Dot in 203 DPI such as TX400, T400, D420, D220, SRP-770, SRP-770II
'        '1 mm   : 12 Dot in 300 DPI such as TX403, T403, D423, D223
'        '4 Inch : 25.4  * 4 * 8 = 812.8
'        '6 Inch : 25.4  * 4 * 8 = 1219.2
'
        'SetPaper(16, 16, 813, 1220, GAP, 0, 16); ' 4 inch (Width) * 6 inch (Hiehgt)
        bResult = SetPaper(nMarginX, nMarginY, nPaper_Width, nPaper_Height, nSensorType, 0, 16) ' 4 inch (Width) * 6 inch (Hiehgt)
        PrintDirect ("STt")
        '   Clear Buffer of Printer
        bResult = ClearBuffer()
'
'
'        lblTest.Caption = Trim(Label1)
'        lblCode.Caption = Trim(code1)
'        w = (lblTest.Width / 15)
'        wn = (w - 4) / (87 - 4)
'        nv = (16) - (16 - 4.5) * wn
'        w1 = (lblCode.Width / 15)
'        wn1 = (w1 - 4) / (87 - 4)
'        nv1 = (16) - (16 - 4.5) * wn1
'        hgap = 38 * 8
'        lblTest1.Caption = Trim(Label2)
'        lblCode1.Caption = Trim(code2)
'        w = (lblTest.Width / 15)
'        wn = (w - 4) / (87 - 4)
'        nv = (16) - (16 - 4.5) * wn
'        w1 = (lblCode.Width / 15)
'        wn1 = (w1 - 4) / (87 - 4)
'        nv1 = (16) - (16 - 4.5) * wn1
'        'Y riga 1
'        myR1Y = 65 + 5
'        'Y riga 2
'        myR2Y = 105 + 5


        bResult = PrintTrueFontLib(0, 13 * MM2D, "Myriad Pro Cond", 30, ROTATE_0, False, True, False, row1)
        
        bResult = PrintTrueFontLib(0, 24 * MM2D, "Myriad Pro Cond", 20, ROTATE_0, False, True, False, row2)
        
        bResult = PrintTrueFontLib(40 * MM2D, 40 * MM2D, "Impact", 120, ROTATE_0, False, True, False, Format(prog, "00"))
         
        
              '  bResult = PrintTrueFontLib(8, 110, "Myriad Pro Cond", 10, 0, False, True, False, Trim("Hello"))
                
                
                
        bResult = Print1DBarcode(0, 72 * MM2D, CODE128, 3, 6, 80, ROTATE_0, True, Format(code, "00000"))
   
      
      
        '   Print Command
        bResult = Prints(1, 1)
        '   Set the Label End
        EndLabel
        '   Disconnect Printer Driver
        bResult = DisconnectPrinter()
  
    
    


End Sub

Private Function autoSizeText(myLabel As Label, myText As String) As Single
    
    Dim w As Single
    Dim wn As Single

        myLabel.Caption = Trim(myText)
        w = (myLabel.Width / 15)
        wn = (w - 4) / (87 - 4)
        autoSizeText = (16) - (16 - 4.5) * wn
    
End Function

Private Sub cmdPrintLabels_Click()

    
    Dim i As Integer
    Dim isLeftItem As Boolean
    Dim codeLeft As String
    Dim codeRight As String
    Dim labelLeft As String
    Dim labelRight As String
    Dim myLabel As String
    Dim myLabel1 As String
    Dim myLabel2 As String
    
    
    
    Select Case printLabelMode
    Case 0 'Etichetta singola con barcode (in base alla selezione)==pu� stampare una etichetta vuota se dispari
        isLeftItem = True
        For i = 1 To Me.lvArticoli.ListItems.Count
            If Me.lvArticoli.ListItems(i).Selected = True Then
                myLabel = Me.lvArticoli.ListItems(i).ListSubItems(2)
                If myLabel = "" Then myLabel = "[" & Me.lvArticoli.ListItems(i).Text & "]"
                If isLeftItem Then
                    labelLeft = myLabel
                    codeLeft = Me.lvArticoli.ListItems(i).Text
                    isLeftItem = False
                Else
                    labelRight = myLabel
                    codeRight = Me.lvArticoli.ListItems(i).Text
                    printLabels codeLeft, labelLeft, codeRight, labelRight, printLabelMode
                    isLeftItem = True
                End If
                updateLastUpdate Me.lvArticoli.ListItems(i).Text
            End If
        Next i
        If Not isLeftItem Then
            printLabels codeLeft, labelLeft, "", "", printLabelMode
        End If
    Case 1 'Etichetta doppia con bacode (due etichette per selezione)== sempre coppia di etichette
        For i = 1 To Me.lvArticoli.ListItems.Count
            If Me.lvArticoli.ListItems(i).Selected = True Then
                myLabel = Me.lvArticoli.ListItems(i).ListSubItems(2)
                If myLabel = "" Then myLabel = "[" & Me.lvArticoli.ListItems(i).Text & "]"
                printLabels Me.lvArticoli.ListItems(i).Text, myLabel, Me.lvArticoli.ListItems(i).Text, myLabel, printLabelMode
                updateLastUpdate Me.lvArticoli.ListItems(i).Text
            End If
            
        Next i
    Case 2 'Etichetta singola barcode a SX(in base a selezione) + etichtta testo libero a DX == sempre coppia di etichette
        For i = 1 To Me.lvArticoli.ListItems.Count
            If Me.lvArticoli.ListItems(i).Selected = True Then
                myLabel = Me.lvArticoli.ListItems(i).ListSubItems(2)
                If myLabel = "" Then myLabel = "[" & Me.lvArticoli.ListItems(i).Text & "]"
                labelLeft = myLabel
                codeLeft = Me.lvArticoli.ListItems(i).Text
                codeRight = Trim(cleanSQL(txtCodeDX.Text))
                labelRight = Trim(cleanSQL(txtLabelDX.Text))
                If codeRight = "" Or labelRight = "" Then
                    MsgBox "Controlla i campi!", vbInformation, Me.Caption
                    Exit Sub
                End If
                printLabels codeLeft, labelLeft, codeRight, labelRight, printLabelMode
                updateLastUpdate Me.lvArticoli.ListItems(i).Text
            End If
        Next i
    Case 3 'Etichetta barcode a SX e etichetta Accessori a DX (in base a selezione)== sempre coppia di etichette
        For i = 1 To Me.lvArticoli.ListItems.Count
            If Me.lvArticoli.ListItems(i).Selected = True Then
                myLabel = Me.lvArticoli.ListItems(i).ListSubItems(2)
                If myLabel = "" Then myLabel = "[" & Me.lvArticoli.ListItems(i).Text & "]"
                labelLeft = myLabel
                codeLeft = Me.lvArticoli.ListItems(i).Text
                codeRight = Me.lvArticoli.ListItems(i).ListSubItems(3)
                If Trim(cleanSQL(txtCodeDX.Text)) <> "" Then codeRight = Trim(cleanSQL(txtCodeDX.Text)) & " " & codeRight
                If codeRight = "" Then codeRight = "Marca"
                labelRight = Me.lvArticoli.ListItems(i).ListSubItems(4)
                If Trim(cleanSQL(txtLabelDX.Text)) <> "" Then labelRight = Trim(cleanSQL(txtLabelDX.Text)) & " " & labelRight
                If labelRight = "" Then labelRight = "Modello"
                printLabels codeLeft, labelLeft, codeRight, labelRight, printLabelMode
                updateLastUpdate Me.lvArticoli.ListItems(i).Text
            End If
        Next i
    Case 4 'Etichetta con testo libero a SX + Etichetta con testo libero a DX (indipendente da selezione). Se testo DX vuoto fa uguale a SX== sempre coppia di etichette
        codeLeft = Trim(cleanSQL(txtCodeSX.Text))
        labelLeft = Trim(cleanSQL(txtLabelSX.Text))
        If codeLeft = "" Or labelLeft = "" Then
            MsgBox "Controlla i campi!", vbInformation, Me.Caption
            Exit Sub
        End If
        codeRight = Trim(cleanSQL(txtCodeDX.Text))
        labelRight = Trim(cleanSQL(txtLabelDX.Text))
        If codeRight = "" Or labelRight = "" Then
            MsgBox "Controlla i campi!", vbInformation, Me.Caption
            Exit Sub
        End If
        printLabels codeLeft, labelLeft, codeRight, labelRight, printLabelMode
    Case 5 'etichetta compatta che scrive "studio visio" in alto e l'ID in basso
        isLeftItem = True
        For i = 1 To Me.lvArticoli.ListItems.Count
            If Me.lvArticoli.ListItems(i).Selected = True Then
                myLabel = Me.lvArticoli.ListItems(i).Text
                If isLeftItem Then
                    labelLeft = myLabel
                    codeLeft = "Studio Visio"
                    isLeftItem = False
                Else
                    labelRight = myLabel
                    codeRight = "Studio Visio"
                    printLabels codeLeft, labelLeft, codeRight, labelRight, printLabelMode
                    isLeftItem = True
                End If
                updateLastUpdate Me.lvArticoli.ListItems(i).Text
            End If
        Next i
        If Not isLeftItem Then
            printLabels codeLeft, labelLeft, "", "", printLabelMode
        End If
        Case 6 ' Etichetta PVC Bianca
        
         For i = 1 To Me.lvArticoli.ListItems.Count
            If Me.lvArticoli.ListItems(i).Selected = True Then
            
            Dim code As String
            Dim row1 As String
            Dim row2 As String
            
            code = Me.lvArticoli.ListItems(i).Text
            row1 = Me.lvArticoli.ListItems(i).ListSubItems(3)
            row2 = Me.lvArticoli.ListItems(i).ListSubItems(4) & " - " & Me.lvArticoli.ListItems(i).ListSubItems(5)
            
               printPVC10x10Label code, row1, row2, Me.lvArticoli.ListItems(i).ListSubItems(11)
               
                'updateLastUpdate Me.lvArticoli.ListItems(i).Text   ????
            End If
        Next i
        Case 7 'Etichetta con testo libero a SX + Etichetta con testo libero a DX (indipendente da selezione). Se testo DX vuoto fa uguale a SX== sempre coppia di etichette Solo un campo centrale
            'codeLeft = Trim(cleanSQL(txtCodeSX.Text))
         Dim myStart As Integer
         Dim myEnd As Integer
         myStart = Val(Trim(cleanSQL(txtLabelSX.Text)))
         myEnd = Val(Trim(cleanSQL(txtLabelDX.Text)))
        If myStart < myEnd Then
            isLeftItem = True
            For i = myStart To myEnd
                myLabel = Format$(i)
                If isLeftItem Then
                    labelLeft = myLabel
                    codeLeft = ""
                    isLeftItem = False
                Else
                    labelRight = myLabel
                    codeRight = ""
                    printLabels codeLeft, labelLeft, codeRight, labelRight, printLabelMode
                    isLeftItem = True
                End If
            Next i
            If Not isLeftItem Then
                printLabels codeLeft, labelLeft, "", "", printLabelMode
            End If
        End If
    End Select
End Sub
Private Sub updateLastUpdate(myID As Integer)
    Dim mySQL As String
    Dim myRS As New ADODB.Recordset
    Dim lastUpdate As String

    lastUpdate = Format(Now(), "YYYYMMDDhhmmss")
    openMysqlDB ("updateLastUpdate")
    mySQL = "UPDATE materiale SET printLastLabel='" & lastUpdate & "' WHERE IDMateriale=" & myID
    myDB.Execute mySQL
    closeMysqlDB "updateLastUpdate"
End Sub

Private Sub Command2_Click()

    creaProgressivi

End Sub

Private Sub Form_Activate()
    'loadGruppi
    Dim i As Integer
    loadArticles Text1.Text
    If selectedItem > 0 And Me.lvArticoli.ListItems.Count > 0 Then
        For i = 1 To Me.lvArticoli.ListItems.Count
       If lvArticoli.ListItems(i).Text = Format(selectedItem, "00000") Then
            lvArticoli.ListItems(i).Selected = True
            lvArticoli.ListItems(i).EnsureVisible
        Else
            lvArticoli.ListItems(i).Selected = False
       End If
    Next i
    End If
End Sub

Private Sub Form_Load()

    Dim i As Integer
    

    lvArticoli.ListItems.Clear
    lvArticoli.HideColumnHeaders = False
    lvArticoli.View = lvwReport
    lvArticoli.ColumnHeaders.Clear
    lvArticoli.ColumnHeaders.Add , , "ID", 600, lvwColumnLeft
    lvArticoli.ColumnHeaders.Add , , "OldCode", 800, lvwColumnLeft
    lvArticoli.ColumnHeaders.Add , , "Label", 1500, lvwColumnLeft
    lvArticoli.ColumnHeaders.Add , , "Gruppo", 1800, lvwColumnLeft
    lvArticoli.ColumnHeaders.Add , , "Marca", 1600, lvwColumnLeft
    lvArticoli.ColumnHeaders.Add , , "modello", 1800, lvwColumnLeft
    lvArticoli.ColumnHeaders.Add , , "note", 4000, lvwColumnLeft
    lvArticoli.ColumnHeaders.Add , , "prezzo", 1000, lvwColumnLeft
    lvArticoli.ColumnHeaders.Add , , "D", 300, lvwColumnLeft
    lvArticoli.ColumnHeaders.Add , , "LastPrint", 1400, lvwColumnLeft
    lvArticoli.ColumnHeaders.Add , , "Tipo", 400, lvwColumnLeft
    lvArticoli.ColumnHeaders.Add , , "Prog", 400, lvwColumnLeft
    For i = 1 To lvArticoli.ColumnHeaders.Count
        lvArticoli.ColumnHeaders(i).Tag = "A"
    Next i
    frmSX.Enabled = False
    frmDX.Enabled = False
    printLabelMode = 0
    loadGruppi
    loadMagazzini
    loadTipi
    cmdPrintLabels.Enabled = False
    
    
End Sub

Private Sub lvArticoli_AfterLabelEdit(Cancel As Integer, NewString As String)
Cancel = 1
End Sub

Private Sub lvArticoli_BeforeLabelEdit(Cancel As Integer)
Cancel = 1
End Sub

Private Sub lvArticoli_Click()
    Dim i As Integer

    For i = 1 To Me.lvArticoli.ListItems.Count
        If Me.lvArticoli.ListItems(i).Selected = True Then Me.cmdPrintLabels.Enabled = True
    Next i
    
End Sub

Private Sub lvArticoli_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)

    If lvArticoli.ColumnHeaders(ColumnHeader.Index).Tag = "A" Then
        lvArticoli.ColumnHeaders(ColumnHeader.Index).Tag = "D"
        lvArticoli.SortOrder = lvwAscending
    Else
        lvArticoli.ColumnHeaders(ColumnHeader.Index).Tag = "A"
        lvArticoli.SortOrder = lvwDescending
    End If
    
    lvArticoli.Sorted = True
    lvArticoli.SortKey = ColumnHeader.Index - 1
    
End Sub
Private Sub loadGruppi()

    Dim mySQL As String
    Dim myRS As New ADODB.Recordset
    cmbLoading = True
    cmbCategoria.Clear
    cmbFiltroCategoria.Clear
    openMysqlDB ("loadGruppi")
    cmbFiltroCategoria.AddItem "- TUTTI -"
    cmbFiltroCategoria.ItemData(cmbFiltroCategoria.NewIndex) = 0
    mySQL = "SELECT * FROM gruppiMateriale WHERE idParent=0 ORDER BY descrizione"
    myRS.Open mySQL, myDB, adOpenForwardOnly, adLockReadOnly
    While Not myRS.EOF
        cmbCategoria.AddItem "" & Trim(myRS("descrizione"))
        cmbFiltroCategoria.AddItem "" & Trim(myRS("descrizione"))
        cmbCategoria.ItemData(cmbCategoria.NewIndex) = myRS("idGruppoMateriale")
        cmbFiltroCategoria.ItemData(cmbFiltroCategoria.NewIndex) = myRS("idGruppoMateriale")
    myRS.MoveNext
    Wend
    myRS.Close
    cmbCategoria.ListIndex = 0
    cmbFiltroCategoria.ListIndex = 0
    cmbLoading = False
    
End Sub
Private Sub loadMagazzini()

    Dim mySQL As String
    Dim myRS As New ADODB.Recordset
    cmbLoading = True
    cmbFiltroMagazzini.Clear
    cmbMagazzini.Clear
    openMysqlDB ("loadGruppi")
    cmbFiltroMagazzini.AddItem "- TUTTI -"
    cmbMagazzini.AddItem "- TUTTI -"
    cmbFiltroMagazzini.ItemData(cmbFiltroMagazzini.NewIndex) = 0
    cmbMagazzini.ItemData(cmbMagazzini.NewIndex) = 0
    mySQL = "SELECT * FROM magazzini"
    myRS.Open mySQL, myDB, adOpenForwardOnly, adLockReadOnly
    While Not myRS.EOF
        cmbFiltroMagazzini.AddItem "" & Trim(myRS("mg_nome"))
        cmbMagazzini.AddItem "" & Trim(myRS("mg_nome"))
        cmbFiltroMagazzini.ItemData(cmbFiltroMagazzini.NewIndex) = myRS("id_mg")
        cmbMagazzini.ItemData(cmbMagazzini.NewIndex) = myRS("id_mg")
    myRS.MoveNext
    Wend
    myRS.Close
    cmbFiltroMagazzini.ListIndex = 0
    cmbMagazzini.ListIndex = 0
    cmbLoading = False
    
End Sub
Private Sub loadTipi()

    Dim mySQL As String
    Dim myRS As New ADODB.Recordset
    cmbLoading = True
    cmbFiltroTipo.Clear
    cmbTipo.Clear
    openMysqlDB ("loadTipi")
    cmbFiltroTipo.AddItem "- TUTTI -"
    cmbTipo.AddItem "- TUTTI -"
    cmbFiltroTipo.ItemData(cmbFiltroTipo.NewIndex) = 0
    cmbTipo.ItemData(cmbTipo.NewIndex) = 0
    mySQL = "SELECT * FROM tipo"
    myRS.Open mySQL, myDB, adOpenForwardOnly, adLockReadOnly
    While Not myRS.EOF
        cmbFiltroTipo.AddItem "" & Trim(myRS("t_tipo"))
        cmbTipo.AddItem "" & Trim(myRS("t_tipo"))
        cmbFiltroTipo.ItemData(cmbFiltroTipo.NewIndex) = myRS("id_t")
        cmbTipo.ItemData(cmbTipo.NewIndex) = myRS("id_t")
    myRS.MoveNext
    Wend
    myRS.Close
    cmbFiltroTipo.ListIndex = 0
    cmbTipo.ListIndex = 0
    cmbLoading = False
    
End Sub


Private Sub creaProgressivi()

    Dim mySQL As String
    Dim myRS As New ADODB.Recordset
    Dim myRS1 As New ADODB.Recordset


' Spenta per evitare sovrascritture una volta eseguita la prima volta

'    openMysqlDB ("creaProgressivi")
'
'    mySQL = "SELECT * FROM gruppiMateriale Group By descrizione ORDER BY descrizione"
'    myRS.Open mySQL, myDB, adOpenForwardOnly, adLockReadOnly
'    While Not myRS.EOF
'
'        mySQL = "SELECT * FROM materiale WHERE IDGruppoMateriale=" & myRS("IDGruppoMateriale") & "  ORDER BY IDMateriale"
'
'        Dim progressivo As Integer
'        progressivo = 1
'        myRS1.Open mySQL, myDB, adOpenForwardOnly, adLockReadOnly
'        While Not myRS1.EOF
'
'
'        mySQL = "UPDATE materiale SET progressivo=" & progressivo & " WHERE IDMateriale = " & myRS1("IDMateriale")
'        myDB.Execute mySQL
'
'
'        Debug.Print (myRS1("modello"))
'
'
'          progressivo = progressivo + 1
'            myRS1.MoveNext
'        Wend
'        myRS1.Close
'
'        myRS.MoveNext
'    Wend
'    myRS.Close

End Sub


Private Sub lvArticoli_DblClick()
Dim mySelectedID As Integer
mySelectedID = Val(Me.lvArticoli.selectedItem)
frmEdit.searchID = mySelectedID
frmEdit.Show 1
End Sub
Private Sub textONOFF(myStateSX As Boolean, myStateDX As Boolean, Optional myDisabledFields As String)
    
    frmSX.Enabled = myStateSX
    frmDX.Enabled = myStateDX

    txtCodeSX.Enabled = myStateSX
    txtCodeDX.Enabled = myStateDX
    txtLabelDX.Enabled = myStateDX
    txtLabelSX.Enabled = myStateSX
    If myDisabledFields = "T" Then 'disable top fields
        txtCodeSX.Enabled = False
        txtCodeDX.Enabled = False
    ElseIf myDisabledFields = "B" Then 'disable bottom fields
        txtLabelDX.Enabled = False
        txtLabelSX.Enabled = False
    Else
    
    End If
   
    txtCodeSX.Text = ""
    txtCodeDX.Text = ""
    txtLabelDX.Text = ""
    txtLabelSX.Text = ""
End Sub

Private Sub optLabelsOption_Click(Index As Integer)

printLabelMode = Index
Select Case Index
Case 0 'Barcode x 1
    textONOFF False, False
    optLabelsOption.Item(1).Value = False
    optLabelsOption.Item(2).Value = False
    optLabelsOption.Item(3).Value = False
    optLabelsOption.Item(4).Value = False
    optLabelsOption.Item(5).Value = False
    optLabelsOption.Item(6).Value = False
    optLabelsOption.Item(7).Value = False
Case 1 'Barcode x 2
    optLabelsOption.Item(0).Value = False
    optLabelsOption.Item(2).Value = False
    optLabelsOption.Item(3).Value = False
    optLabelsOption.Item(4).Value = False
    optLabelsOption.Item(5).Value = False
    optLabelsOption.Item(6).Value = False
    optLabelsOption.Item(7).Value = False
    textONOFF False, False
Case 2 'Barcode + testo libero
    optLabelsOption.Item(0).Value = False
    optLabelsOption.Item(1).Value = False
    optLabelsOption.Item(3).Value = False
    optLabelsOption.Item(4).Value = False
    optLabelsOption.Item(5).Value = False
    optLabelsOption.Item(6).Value = False
    optLabelsOption.Item(7).Value = False
    textONOFF False, True
Case 3 'barcode + accessori
    optLabelsOption.Item(0).Value = False
    optLabelsOption.Item(1).Value = False
    optLabelsOption.Item(2).Value = False
    optLabelsOption.Item(4).Value = False
    optLabelsOption.Item(5).Value = False
    optLabelsOption.Item(6).Value = False
    optLabelsOption.Item(7).Value = False
    textONOFF False, True
Case 4 'testo libero
    optLabelsOption.Item(0).Value = False
    optLabelsOption.Item(1).Value = False
    optLabelsOption.Item(2).Value = False
    optLabelsOption.Item(3).Value = False
    optLabelsOption.Item(5).Value = False
    optLabelsOption.Item(6).Value = False
    optLabelsOption.Item(7).Value = False
    textONOFF True, True
Case 5 '
    optLabelsOption.Item(0).Value = False
    optLabelsOption.Item(1).Value = False
    optLabelsOption.Item(2).Value = False
    optLabelsOption.Item(3).Value = False
    optLabelsOption.Item(4).Value = False
    optLabelsOption.Item(6).Value = False
    optLabelsOption.Item(7).Value = False
    textONOFF True, True
Case 6 'PVC bianca
    optLabelsOption.Item(0).Value = False
    optLabelsOption.Item(1).Value = False
    optLabelsOption.Item(2).Value = False
    optLabelsOption.Item(3).Value = False
    optLabelsOption.Item(4).Value = False
    optLabelsOption.Item(5).Value = False
    optLabelsOption.Item(7).Value = False
    textONOFF True, True
    cmdPrintLabels.Enabled = True
    
Case 7 'testo x votatori
    optLabelsOption.Item(0).Value = False
    optLabelsOption.Item(1).Value = False
    optLabelsOption.Item(2).Value = False
    optLabelsOption.Item(3).Value = False
    optLabelsOption.Item(4).Value = False
    optLabelsOption.Item(5).Value = False
    optLabelsOption.Item(6).Value = False
    textONOFF True, True, "T"
    cmdPrintLabels.Enabled = True
End Select
End Sub

Private Sub optTipo_Click(Index As Integer)
    loadArticles Text1.Text
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        Command1_Click
        Text1.SelStart = 0
        Text1.SelLength = 1000
    End If
    
End Sub

