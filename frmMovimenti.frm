VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMovimenti 
   Caption         =   "Movimenti"
   ClientHeight    =   10800
   ClientLeft      =   60
   ClientTop       =   405
   ClientWidth     =   15855
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   10800
   ScaleWidth      =   15855
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.ListView lvGruppi 
      Height          =   2955
      Left            =   240
      TabIndex        =   14
      Top             =   7620
      Width           =   5355
      _ExtentX        =   9446
      _ExtentY        =   5212
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Frame Frame2 
      Caption         =   "Stampa"
      Height          =   3015
      Left            =   5760
      TabIndex        =   10
      Top             =   7620
      Width           =   3015
      Begin VB.CommandButton cmdPrintScheda 
         Caption         =   "Stampa scheda"
         Height          =   675
         Left            =   180
         TabIndex        =   12
         Top             =   1140
         Width           =   2715
      End
      Begin VB.CommandButton cmdPrintLabel 
         Caption         =   "Stampa etichette uscita"
         Height          =   675
         Left            =   180
         TabIndex        =   11
         Top             =   360
         Width           =   2715
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Note Assegnazione"
      Height          =   3075
      Left            =   8880
      TabIndex        =   7
      Top             =   7560
      Width           =   3555
      Begin VB.CommandButton cmdSalvaNote 
         Caption         =   "Salva Note Assegnazione"
         Height          =   675
         Left            =   180
         TabIndex        =   9
         Top             =   2160
         Width           =   3195
      End
      Begin VB.TextBox txtNoteAssegnazione 
         Height          =   1635
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   8
         Top             =   240
         Width           =   3255
      End
   End
   Begin VB.Frame frmInserimento 
      Caption         =   "Inseriemento articolo"
      Height          =   3075
      Left            =   12540
      TabIndex        =   2
      Top             =   7560
      Width           =   3135
      Begin VB.CommandButton cmdDelete 
         Caption         =   "Cancella movimenti"
         Height          =   675
         Left            =   600
         TabIndex        =   16
         Top             =   2160
         Width           =   1935
      End
      Begin VB.CommandButton cmdInserimento 
         Caption         =   "Inserisci"
         Default         =   -1  'True
         Height          =   675
         Left            =   180
         TabIndex        =   4
         Top             =   1020
         Width           =   2715
      End
      Begin VB.TextBox txtIDMateriale 
         Height          =   375
         Left            =   180
         TabIndex        =   3
         Top             =   480
         Width           =   2715
      End
   End
   Begin MSComctlLib.ListView lvMovimenti 
      Height          =   6135
      Left            =   180
      TabIndex        =   0
      Top             =   1020
      Width           =   15435
      _ExtentX        =   27226
      _ExtentY        =   10821
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Label lblDate 
      Alignment       =   1  'Right Justify
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   375
      Left            =   10260
      TabIndex        =   15
      Top             =   540
      Width           =   5295
   End
   Begin VB.Label lblLuogo 
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   375
      Left            =   240
      TabIndex        =   13
      Top             =   540
      Width           =   8835
   End
   Begin VB.Label lblAlert 
      Alignment       =   1  'Right Justify
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   255
      Left            =   13440
      TabIndex        =   6
      Top             =   7260
      Width           =   2175
   End
   Begin VB.Label lblCount 
      Caption         =   "0 articoli"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   315
      Left            =   300
      TabIndex        =   5
      Top             =   7260
      Width           =   2355
   End
   Begin VB.Label lblMovimenti 
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   375
      Left            =   240
      TabIndex        =   1
      Top             =   120
      Width           =   8835
   End
End
Attribute VB_Name = "frmMovimenti"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public myIDLavoro As Integer
Public myNomeLavoro As String
Public myLuogoLavoro As String
Public myDate As String
Public myQuantita As Integer

Private Sub cmdDelete_Click()
Dim ret As Integer
Dim ret2 As Boolean
Dim message As String
Dim i As Integer
    message = "Vuoi cancellare i movimenti selezionati?"
    ret = MsgBox(message, vbQuestion + vbYesNo, Me.Caption)
    If ret = vbNo Then Exit Sub
    For i = 1 To lvMovimenti.ListItems.Count
    If lvMovimenti.ListItems(i).Selected = True Then
       deleteMovimento Val(lvMovimenti.ListItems(i).Text)
    End If
Next i
 loadMovimenti myIDLavoro
    
End Sub

Private Sub cmdInserimento_Click()
    Dim mySQL As String
    Dim myAssegnazione As String
    If Trim(txtIDMateriale.Text) = "" Then Exit Sub
    cmdInserimento.Enabled = False

    decodeString Trim(txtIDMateriale.Text)
    
    'loadMovimenti myIDLavoro
    'Selezionami l'ultimo della lista
    'lvMovimenti.ListItems(lvMovimenti.ListItems.Count).Selected = True
    cmdInserimento.Enabled = True
    txtIDMateriale.Text = ""
    txtIDMateriale.SetFocus
End Sub
Private Function insMateriale(myAssegnazione As String, myTipo As String, myValQuantita As Integer) As Boolean
    Dim i As Integer
    Dim mySQL As String
    Dim myCodiceQuantita As codiceQuantita
    Dim thihgToDO As Integer '1 inserisci, 2 updata , 3 niente c'� gi�=false
    openMysqlDB ("insMateriale")
    
    myCodiceQuantita = checkRecord(myIDLavoro, CLng(Mid(txtIDMateriale.Text, 1, 5)))
    If myTipo = "G" Then
        If myValQuantita = 0 Then myValQuantita = 1
        myQuantita = myValQuantita + myCodiceQuantita.quantita
        If myCodiceQuantita.quantita > 0 Then
            thihgToDO = 2
        Else
            thihgToDO = 1
        End If
    Else
        If myCodiceQuantita.quantita > 0 Then
            thihgToDO = 3
        Else
            thihgToDO = 1
        End If
    End If
    If myQuantita = 0 Then myQuantita = 1
    Select Case thihgToDO
    Case 1
        mySQL = "INSERT INTO movimenti ("
        mySQL = mySQL & "IDLavoro,"
        mySQL = mySQL & "IDMateriale,"
        mySQL = mySQL & "TSOut,"
        mySQL = mySQL & "Quantita "
    
        mySQL = mySQL & " ) VALUES ( "
        
        mySQL = mySQL & "" & myIDLavoro & ","
        mySQL = mySQL & "" & Val(txtIDMateriale.Text) & ","
        mySQL = mySQL & "'" & Format(Now(), "YYYYMMDDhhmmss") & "',"
        mySQL = mySQL & "" & myQuantita & ")"
        myDB.Execute mySQL
        'lblAlert.Caption = ""
        insMateriale = True
    Case 2
        mySQL = "UPDATE movimenti SET Quantita=" & myQuantita & " WHERE IDMovimento = " & myCodiceQuantita.codice
        myDB.Execute mySQL
        insMateriale = True
    Case 3
        insMateriale = False
    End Select
    txtIDMateriale.Text = ""
    closeMysqlDB ("insMateriale")


End Function
Private Function checkRecord(myIDLavoro As Integer, myIDMateriale As Integer) As codiceQuantita
    Dim mySQL As String
    Dim myRS As New ADODB.Recordset
    mySQL = "SELECT * FROM movimenti WHERE IDLavoro = " & myIDLavoro & " AND  IDMateriale = " & myIDMateriale
    myRS.Open mySQL, myDB, adOpenForwardOnly, adLockReadOnly
    If Not myRS.EOF Then
    checkRecord.quantita = Val(myRS("quantita"))
    checkRecord.codice = myRS("IDMovimento")
        'checkRecord = Val(myRS("quantita")) & "|" & myRS("IDMovimento")
    Else
        checkRecord.codice = 0
        checkRecord.quantita = 0
    End If
    
End Function

Private Sub cmdPrintLabel_Click()

Dim ret As Integer
Dim ret2 As Boolean
Dim message As String
Dim i As Integer
    message = "Vuoi stampare le etichette d'uscita per i movimenti selezionati?"
    ret = MsgBox(message, vbQuestion + vbYesNo, Me.Caption)
    If ret = vbNo Then Exit Sub
    
    For i = 1 To lvMovimenti.ListItems.Count
    If lvMovimenti.ListItems(i).Selected = True Then
'    printExitLabels(myIDMateriale As Integer, myEventName As String, myDestinazione As String, myDescrizione As String, myNote As String) As Boolean
        ret2 = printExitLabels("R" & Format(lvMovimenti.ListItems(i).Text, "000000"), myNomeLavoro, myLuogoLavoro, lvMovimenti.ListItems(i).SubItems(7), lvMovimenti.ListItems(i).SubItems(8))
    End If
Next i


End Sub

Private Sub cmdSalvaNote_Click()
Dim mySelectedLinesCount As Integer
Dim myLastSelected As Integer
Dim i As Integer
If Trim(Me.txtNoteAssegnazione.Text) = "" Then Exit Sub

For i = 1 To lvMovimenti.ListItems.Count
    If lvMovimenti.ListItems(i).Selected = True Then
        mySelectedLinesCount = mySelectedLinesCount + 1
        myLastSelected = Val(lvMovimenti.ListItems(i).Text)
        updateNotaAssegnazione Trim(Me.txtNoteAssegnazione.Text), myLastSelected
    End If
Next i
loadMovimenti myIDLavoro
If mySelectedLinesCount > 1 Then
    For i = 1 To lvMovimenti.ListItems.Count - 1
            lvMovimenti.ListItems(i).Selected = False
    Next i
    lvMovimenti.ListItems(lvMovimenti.ListItems.Count).Selected = True
    lvMovimenti.ListItems(lvMovimenti.ListItems.Count).EnsureVisible
Else
    For i = 1 To lvMovimenti.ListItems.Count
        If lvMovimenti.ListItems(i).Text = myLastSelected Then
            lvMovimenti.ListItems(i).Selected = True
            lvMovimenti.ListItems(i).EnsureVisible
        Else
            lvMovimenti.ListItems(i).Selected = False
        End If
    Next i
End If


End Sub

Private Sub Form_Activate()
    loadMovimenti myIDLavoro
    loadGruppi myIDLavoro
    txtIDMateriale.SetFocus
End Sub

Private Sub Form_Load()
    Dim i As Integer
    lblMovimenti.Caption = "[" & myIDLavoro & "] " & myNomeLavoro
    lblLuogo.Caption = myLuogoLavoro
    lblDate.Caption = myDate
    lvMovimenti.ListItems.Clear
    lvMovimenti.HideColumnHeaders = False
    lvMovimenti.View = lvwReport
    lvMovimenti.ColumnHeaders.Clear
    lvMovimenti.ColumnHeaders.Add , , "ID", 600, lvwColumnLeft
    lvMovimenti.ColumnHeaders.Add , , "IDMateriale", 800, lvwColumnLeft
    lvMovimenti.ColumnHeaders.Add , , "Gruppo", 2000, lvwColumnLeft
    lvMovimenti.ColumnHeaders.Add , , "Label", 1600, lvwColumnLeft
    lvMovimenti.ColumnHeaders.Add , , "Assegnazione", 1200, lvwColumnLeft
    lvMovimenti.ColumnHeaders.Add , , "Nota", 2500, lvwColumnLeft
    lvMovimenti.ColumnHeaders.Add , , "Marca", 1200, lvwColumnLeft
    lvMovimenti.ColumnHeaders.Add , , "Modello", 1600, lvwColumnLeft
    lvMovimenti.ColumnHeaders.Add , , "TSOut", 1500, lvwColumnLeft
    lvMovimenti.ColumnHeaders.Add , , "TSIn", 1500, lvwColumnLeft

    For i = 1 To lvMovimenti.ColumnHeaders.Count
        lvMovimenti.ColumnHeaders(i).Tag = "A"
    Next i
    
    lvGruppi.ListItems.Clear
    lvGruppi.HideColumnHeaders = False
    lvGruppi.View = lvwReport
    lvGruppi.ColumnHeaders.Clear
    lvGruppi.ColumnHeaders.Add , , "Quantit�", 800, lvwColumnLeft
    lvGruppi.ColumnHeaders.Add , , "Categoria", 3000, lvwColumnLeft
    For i = 1 To lvGruppi.ColumnHeaders.Count
        lvGruppi.ColumnHeaders(i).Tag = "A"
    Next i
    
End Sub

Public Sub loadMovimenti(myID As Integer)


    Dim myRS As New ADODB.Recordset
    Dim mySQL As String
    Dim rcordcount As Integer
    
    openMysqlDB ("loadMovimenti")
    
    myRS.CursorLocation = adUseClient
    myRS.CursorType = adOpenKeyset

    mySQL = "SELECT mat.Tipo,grm.descrizione as Gdes,mov.IDMovimento,mov.IDMateriale,mat.marca,mat.modello,mov.TSOut,mov.TSIn,mat.label,ass.descrizione as Mdes,mov.NotaAssegnazione,mov.Quantita FROM movimenti as mov INNER JOIN  materiale as mat ON mov.IDMateriale = mat.IDMateriale LEFT JOIN assegnazioni as ass ON mov.IDAssegnazione = ass.IDAssegnazione LEFT JOIN gruppiMateriale as grm ON mat.idGruppoMateriale = grm.idGruppoMateriale WHERE IDLavoro = " & myID & " ORDER  BY mov.IDMovimento ASC"

    lvMovimenti.ListItems.Clear
    lvMovimenti.Sorted = False
    myRS.Open mySQL, myDB, adOpenForwardOnly, adLockReadOnly
    While Not myRS.EOF
        lvMovimenti.ListItems.Add , "A" & myRS("IDMovimento"), myRS("IDMovimento")
        lvMovimenti.ListItems(lvMovimenti.ListItems.Count).SubItems(1) = Format("" & myRS("IDMateriale"), "00000")
        lvMovimenti.ListItems(lvMovimenti.ListItems.Count).SubItems(2) = IIf(myRS("Quantita") > 1, myRS("Quantita") & " ", "") & "" & myRS("Gdes")
        lvMovimenti.ListItems(lvMovimenti.ListItems.Count).SubItems(3) = "" & myRS("label")
        lvMovimenti.ListItems(lvMovimenti.ListItems.Count).SubItems(4) = "" & myRS("Mdes")
        lvMovimenti.ListItems(lvMovimenti.ListItems.Count).SubItems(5) = "" & myRS("NotaAssegnazione")
        lvMovimenti.ListItems(lvMovimenti.ListItems.Count).SubItems(6) = "" & myRS("marca")
        lvMovimenti.ListItems(lvMovimenti.ListItems.Count).SubItems(7) = "" & myRS("modello")
        lvMovimenti.ListItems(lvMovimenti.ListItems.Count).SubItems(8) = "" & myRS("TSOut")
        lvMovimenti.ListItems(lvMovimenti.ListItems.Count).SubItems(9) = "" & myRS("TSIn")
        lvMovimenti.ListItems(lvMovimenti.ListItems.Count).Tag = "" & myRS("Tipo") & "|" & myRS("Quantita")
        rcordcount = rcordcount + 1
        myRS.MoveNext
    Wend
    'lblArtCount.Caption = lvMovimenti.ListItems.Count & "/" & printedLabels & " Articoli/Etichette - Prezzo tot " & prezzoTotale
'    If lvMovimenti.ListItems.Count > 0 Then
'    lvMovimenti.ListItems(1).Selected = True
'
'    lvMovimenti.ListItems(1).EnsureVisible
'    End If
    lblCount.Caption = rcordcount & " Articoli"
    closeMysqlDB ("loadMovimenti")
    

End Sub

Public Sub loadGruppi(myID As Integer)


    Dim myRS As New ADODB.Recordset
    Dim myRSa As New ADODB.Recordset
    Dim mySQL As String
    Dim mySQLa As String
    Dim rcordcount As Integer
    Dim myIDG As Integer
    Dim myDescrizione As String
    openMysqlDB ("loadGruppi")
    
    myRS.CursorLocation = adUseClient
    myRS.CursorType = adOpenKeyset

    mySQL = "SELECT DISTINCT grm.descrizione as Gdes,grm.idGruppoMateriale as idg FROM movimenti as mov INNER JOIN  materiale as mat ON mov.IDMateriale = mat.IDMateriale LEFT JOIN assegnazioni as ass ON mov.IDAssegnazione = ass.IDAssegnazione LEFT JOIN gruppiMateriale as grm ON mat.idGruppoMateriale = grm.idGruppoMateriale WHERE IDLavoro = " & myID & " ORDER  BY grm.descrizione ASC"

    lvGruppi.ListItems.Clear
    lvGruppi.Sorted = False
    myRS.Open mySQL, myDB, adOpenForwardOnly, adLockReadOnly
    While Not myRS.EOF
        myIDG = myRS("idg")
        myDescrizione = "" & myRS("Gdes")
        rcordcount = 0
        mySQLa = "SELECT mat.idGruppoMateriale,mov.IDMovimento,mov.IDMateriale,mat.marca,mat.modello,mov.TSOut,mov.TSIn,mat.label,ass.descrizione as Mdes,mov.NotaAssegnazione,mov.Quantita FROM movimenti as mov INNER JOIN  materiale as mat ON mov.IDMateriale = mat.IDMateriale LEFT JOIN assegnazioni as ass ON mov.IDAssegnazione = ass.IDAssegnazione WHERE IDLavoro = " & myID & " AND mat.idGruppoMateriale=" & myIDG
        myRSa.Open mySQLa, myDB, adOpenForwardOnly, adLockReadOnly
        While Not myRSa.EOF
            'rcordcount = rcordcount + 1
            rcordcount = rcordcount + myRSa("Quantita")
            myRSa.MoveNext
        Wend
        myRSa.Close
        myRS.MoveNext
        lvGruppi.ListItems.Add , "A" & myIDG, rcordcount
        lvGruppi.ListItems(lvGruppi.ListItems.Count).SubItems(1) = Trim(myDescrizione)
    Wend
    closeMysqlDB ("loadGruppi")
    

End Sub


Private Sub lvMovimenti_AfterLabelEdit(Cancel As Integer, NewString As String)
Cancel = 1
End Sub

Private Sub lvMovimenti_BeforeLabelEdit(Cancel As Integer)
Cancel = 1
End Sub

Private Sub lvMovimenti_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    If lvMovimenti.ColumnHeaders(ColumnHeader.Index).Tag = "A" Then
        lvMovimenti.ColumnHeaders(ColumnHeader.Index).Tag = "D"
        lvMovimenti.SortOrder = lvwAscending
    Else
        lvMovimenti.ColumnHeaders(ColumnHeader.Index).Tag = "A"
        lvMovimenti.SortOrder = lvwDescending
    End If
    
    lvMovimenti.Sorted = True
    lvMovimenti.SortKey = ColumnHeader.Index - 1
End Sub

Private Sub lvMovimenti_DblClick()
    Dim myRS As New ADODB.Recordset
    Dim mySQL As String
    Dim myLastID As Integer
    Dim i As Integer
    myLastID = Val(lvMovimenti.selectedItem.Text)
If Mid(lvMovimenti.selectedItem.Tag, 1, 1) = "G" Then
    frmMovDet.myValue = Val(Mid(lvMovimenti.selectedItem.Tag, 3, 3))
    frmMovDet.Show 1
    If myQuantita > 0 Then
        openMysqlDB ("lvMovimenti_DblClick")
        mySQL = "UPDATE movimenti SET Quantita=" & myQuantita & " WHERE IDMovimento = " & myLastID
        myDB.Execute mySQL
        closeMysqlDB ("lvMovimenti_DblClick")
    Else
        Exit Sub
    End If
    loadMovimenti myIDLavoro
    loadGruppi myIDLavoro
    For i = 1 To lvMovimenti.ListItems.Count
        If lvMovimenti.ListItems(i).Text = myLastID Then
            lvMovimenti.ListItems(i).Selected = True
            lvMovimenti.ListItems(i).EnsureVisible
        Else
            lvMovimenti.ListItems(i).Selected = False
        End If
    Next i
    txtIDMateriale.SetFocus
End If
End Sub

Private Sub txtIDMateriale_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        cmdInserimento_Click
        txtIDMateriale.SelStart = 0
        txtIDMateriale.SelLength = 1000
    End If

End Sub

Private Sub cmdPrintScheda_Click()


    Dim XStart As Integer
    Dim YStart As Integer
    Dim Pagine As Integer
    Dim Pagina As Integer
    Dim nMat As Integer
    Dim Marca As String
    Dim Modello As String
    Dim Label As String
    Dim Label1, Label2, Label3, Label4, Label5 As String
    Dim IDMovimento As String
    Dim i, c, l, t, j As Integer
    Dim dataOra As String
    Dim PrinterFound As Boolean
    Dim found As String
    Dim Record2print As Integer
    Dim Conteggio As String
    Dim Gruppo As String
    
    found = ""
    If PrinterStandard <> "" Then
        For i = 1 To Printers.Count - 1
            If Printers(i).DeviceName = PrinterStandard Then
                    found = PrinterStandard
                    Set Printer = Printers(i)
                    Exit For
            End If
        Next i
    End If
    If found = "" Then
        MsgBox "Stampante Non trovata!"
        Exit Sub
    End If
    Record2print = 0
    For i = 1 To Me.lvMovimenti.ListItems.Count
        If Me.lvMovimenti.ListItems(i).SubItems(6) = "" Then
            Record2print = Record2print + 1
        End If
    Next i
    Record2print = Record2print + lvGruppi.ListItems.Count

    dataOra = Format$(Now, "dd/mm/yy hh:mm:ss")
    c = Record2print

    Pagine = 1 + Int((c - 1) / 44)
    
    Pagina = 1
    GoSub STP_Intestazione
    
    
    
    
    For i = 1 To lvGruppi.ListItems.Count
        nMat = nMat + 1.25
        Conteggio = lvGruppi.ListItems(i).Text
        Gruppo = lvGruppi.ListItems(i).SubItems(1)
        GoSub STP_Conteggio
    Next i
    nMat = CInt(nMat) + 1
    
    YStart = YStart + 350
    
    For i = 1 To Me.lvMovimenti.ListItems.Count
        'If Me.lvMovimenti.ListItems(i).SubItems(6) <> "" Then
            If nMat = 50 Then
                Pagina = Pagina + 1
                nMat = 0
                Printer.NewPage
                GoSub STP_Intestazione
            End If
    
            nMat = nMat + 1
            Label1 = Me.lvMovimenti.ListItems(i).SubItems(1)
            If Trim(Me.lvMovimenti.ListItems(i).SubItems(2)) = "non definito" Then
                Label2 = "nd-" & Me.lvMovimenti.ListItems(i).SubItems(6) & " " & Me.lvMovimenti.ListItems(i).SubItems(7)
            Else
                Label2 = Trim(Me.lvMovimenti.ListItems(i).SubItems(2))
            End If
            Label3 = Me.lvMovimenti.ListItems(i).SubItems(3)
            Label4 = Me.lvMovimenti.ListItems(i).SubItems(4)
            Label5 = Me.lvMovimenti.ListItems(i).SubItems(5)
            
'            Label = Me.lvMovimenti.ListItems(i).SubItems(2)
'            Marca = Me.lvMovimenti.ListItems(i).SubItems(3)
'            Modello = Me.lvMovimenti.ListItems(i).SubItems(4)
            IDMovimento = Me.lvMovimenti.ListItems(i).Text

            GoSub STP_Computer
        'End If
    Next i
    
    Printer.EndDoc
    Exit Sub
    
    
STP_Conteggio:

    XStart = 0
    YStart = YStart + 350
    Printer.Line (XStart, YStart)-(800, YStart + 350), , B
    Printer.CurrentX = XStart + 100: Printer.CurrentY = YStart + 70
    Printer.FontName = "Tahoma": Printer.FontSize = 10
    Printer.Print Conteggio
    XStart = XStart + 800
    
    Printer.Line (XStart, YStart)-(XStart + 2800, YStart + 350), , B
    Printer.CurrentX = XStart + 100: Printer.CurrentY = YStart + 70
    Printer.FontName = "Tahoma": Printer.FontSize = 10
    Printer.Print Gruppo
    XStart = XStart + 2800

    Return
    
STP_Computer:
'    XStart = IIf(nMat Mod 2 = 0, 5500, 0)
'    YStart = Int((nMat - 1) / 2) * 1200 + 800
'    Printer.Line (XStart, YStart)-(XStart + 5500, YStart + 1200), , B
'    Printer.CurrentX = XStart + 200: Printer.CurrentY = YStart + 350
'    Printer.FontName = "Tahoma": Printer.FontSize = 10
'    Printer.Print "Marca: " & Marca & ""
'    Printer.CurrentX = XStart + 4000: Printer.CurrentY = YStart + 200
'    Printer.FontName = "Tahoma": Printer.FontSize = 10
'    Printer.Print "Modello: " & Modello
'    Printer.CurrentX = XStart + 4000: Printer.CurrentY = YStart + 500
'    Printer.FontName = "Tahoma": Printer.FontSize = 10
'    Printer.Print "ID : " & IDMateriale
'    Printer.CurrentX = XStart + 200: Printer.CurrentY = YStart + 950
'    Printer.FontName = "Tahoma": Printer.FontSize = 9
'    Printer.Print ComNote

    XStart = 0
    YStart = YStart + 300
    Printer.Line (XStart, YStart)-(700, YStart + 300), , B
    Printer.CurrentX = XStart + 50: Printer.CurrentY = YStart + 50
    Printer.FontName = "Tahoma": Printer.FontSize = 8
    Printer.Print Label1
    
    XStart = XStart + 700
    Printer.Line (XStart, YStart)-(XStart + 2200, YStart + 300), , B
    Printer.CurrentX = XStart + 50: Printer.CurrentY = YStart + 50
    Printer.FontName = "Tahoma": Printer.FontSize = 8
    Printer.Print Label2
    
    XStart = XStart + 2200
    Printer.Line (XStart, YStart)-(XStart + 1800, YStart + 300), , B
    Printer.CurrentX = XStart + 50: Printer.CurrentY = YStart + 50
    Printer.FontName = "Tahoma": Printer.FontSize = 8
    Printer.Print Label3
    
    XStart = XStart + 1800
    Printer.Line (XStart, YStart)-(XStart + 1800, YStart + 300), , B
    Printer.CurrentX = XStart + 50: Printer.CurrentY = YStart + 50
    Printer.FontName = "Tahoma": Printer.FontSize = 8
    Printer.Print Label4 & " " & Label5
    
    XStart = XStart + 1800
    Printer.Line (XStart, YStart)-(XStart + 4500, YStart + 300), , B
    Printer.CurrentX = XStart + 50: Printer.CurrentY = YStart + 50
    Printer.FontName = "Tahoma": Printer.FontSize = 8
    Printer.Print Label4 & " " & Label5

    Return

STP_Intestazione:
    Printer.FontName = "Tahoma"
    Printer.FontSize = 12: Printer.FontBold = True
    Printer.CurrentX = 0: Printer.CurrentY = 0
    Printer.Print "Evento: " & lblMovimenti.Caption
    
    Printer.FontName = "Tahoma"
    Printer.FontSize = 10: Printer.FontBold = False
    Printer.CurrentX = 0: Printer.CurrentY = 400
    Printer.Print lblDate.Caption & " " & Me.lblLuogo.Caption
    
    Printer.FontSize = 12: Printer.FontBold = False
    Printer.CurrentX = 10000: Printer.CurrentY = 0
    Printer.Print "Tot:" & Format$(c)
    Printer.CurrentX = 10000: Printer.CurrentY = 400
    Printer.Print "Pag:" & Pagina & "/" & Pagine

    YStart = 500

    Printer.FontSize = 8: Printer.FontBold = False
    Printer.CurrentY = 16000: Printer.CurrentX = 5000
    Printer.Print "Data stampa : " & dataOra
Return

End Sub
Private Function lvSelectionState() As Integer
'restituisce 0 se non � selezionato nulle, -1 se una multiselezione e l'id selezionato se uno solo
    Dim i As Integer
    lvSelectionState = 0
    For i = 1 To lvMovimenti.ListItems.Count
        If lvMovimenti.ListItems(i).Selected = True Then
            If lvSelectionState > 0 Then
                lvSelectionState = -1
                Exit For
            Else
                lvSelectionState = Val(lvMovimenti.ListItems(i).Text)
            End If
        End If
    Next i
End Function
Private Sub decodeString(myString As String)

    Dim myAssegnazione As String
    Dim myIDAssegnazione As Integer
    Dim mySelectedID As Integer
    Dim myTipo As String
    Dim i As Integer
      
If Mid(myString, 1, 1) = "A" And Len(myString) = 4 Then
    'ho beggiato un codice assegnazione
    mySelectedID = lvSelectionState
    myIDAssegnazione = Val(Mid(myString, 2, 3))
    myAssegnazione = getAssegnazione(myIDAssegnazione)
    If myAssegnazione <> "" And mySelectedID > 0 Then
    '� un codice assegnazione valido e ho un solo articolo selezionato da assegnare: updato
        updateAsseganzione myIDAssegnazione, mySelectedID
    'ricarico la lista e mi piazzo sul record appena aggiornato
        loadMovimenti myIDLavoro
        For i = 1 To lvMovimenti.ListItems.Count
            If lvMovimenti.ListItems(i).Text = mySelectedID Then
                lvMovimenti.ListItems(i).Selected = True
                lvMovimenti.ListItems(i).EnsureVisible
            Else
                lvMovimenti.ListItems(i).Selected = False
            End If
        Next i
    ElseIf myAssegnazione <> "" And mySelectedID < 0 Then
        '� un codice assegnazione valido e ho pi� codici articolo da assegnare: ciclo di update
         For i = 1 To lvMovimenti.ListItems.Count
            If lvMovimenti.ListItems(i).Selected = True Then
                updateAsseganzione myIDAssegnazione, Val(lvMovimenti.ListItems(i).Text)
            End If
        Next i
        loadMovimenti myIDLavoro
    Else
    'ho un codice non trovato oppure non ho selezionato nessuna riga: seleziono l'ultima
        lvMovimenti.ListItems(lvMovimenti.ListItems.Count).Selected = True
        lvMovimenti.ListItems(lvMovimenti.ListItems.Count).EnsureVisible
    End If
ElseIf Mid(myString, 1, 1) = "R" And Len(myString) = 7 Then
  'Ho beggiato la label con l'id del moviemento preceduta da R es R000000 per gestire il rientro
   MsgBox ("Etichetta di rientro beggiata: Ancora da gestire")
ElseIf Mid(myString, 6, 1) = "X" And Len(myString) = 9 Then
   'ho beggiato un codice con un articolo generico e la sua quantit�
   myTipo = getTipoMateriale(Val(Mid(myString, 1, 5)))
    If myTipo = "G" Then
        If insMateriale(Val(myString), myTipo, (Val(Mid(myString, 7, 3)))) = True Then
            'ho inserito un elemento ricarico la lista e mi piazzo sull'ultimo
            loadMovimenti myIDLavoro
            For i = 1 To lvMovimenti.ListItems.Count - 1
                    lvMovimenti.ListItems(i).Selected = False
            Next i
            lvMovimenti.ListItems(lvMovimenti.ListItems.Count).Selected = True
            lvMovimenti.ListItems(lvMovimenti.ListItems.Count).EnsureVisible
        Else
            'l'elemento esiste gi� e lo seleziono
            For i = 1 To lvMovimenti.ListItems.Count
                If lvMovimenti.ListItems(i).SubItems(1) = Val(myString) Then
                    lvMovimenti.ListItems(i).Selected = True
                    lvMovimenti.ListItems(i).EnsureVisible
                Else
                    lvMovimenti.ListItems(i).Selected = False
                End If
            Next i
        End If
    Else
        MsgBox "Errore nella formattazione del codice!", vbCritical, Me.Caption
    End If
ElseIf Val(myString) > 0 Then
'ho beggiato un codice numerico
    myTipo = getTipoMateriale(Val(myString))
    If insMateriale(Val(myString), myTipo, 0) = True Then
        'ho inserito un elemento ricarico la lista e mi piazzo sull'ultimo
        loadMovimenti myIDLavoro
        For i = 1 To lvMovimenti.ListItems.Count - 1
                lvMovimenti.ListItems(i).Selected = False
        Next i
        lvMovimenti.ListItems(lvMovimenti.ListItems.Count).Selected = True
        lvMovimenti.ListItems(lvMovimenti.ListItems.Count).EnsureVisible
    Else
        'l'elemento esiste gi� e lo seleziono
        For i = 1 To lvMovimenti.ListItems.Count
            If lvMovimenti.ListItems(i).SubItems(1) = Val(myString) Then
                lvMovimenti.ListItems(i).Selected = True
                lvMovimenti.ListItems(i).EnsureVisible
            Else
                lvMovimenti.ListItems(i).Selected = False
            End If
        Next i
    End If
End If


End Sub

Private Sub updateAsseganzione(myIDAssegnazione As Integer, myIDMovimento As Integer)
    Dim myRS As New ADODB.Recordset
    Dim mySQL As String
    On Error GoTo hell
    openMysqlDB ("updateAsseganzione")
    mySQL = "UPDATE movimenti SET IDAssegnazione=" & myIDAssegnazione & " WHERE IDMovimento = " & myIDMovimento
    myDB.Execute mySQL
    closeMysqlDB ("updateAsseganzione")
    Exit Sub
hell:
    
End Sub
Private Sub updateNotaAssegnazione(myText As String, myIDMovimento As Integer)
    Dim myRS As New ADODB.Recordset
    Dim mySQL As String
    On Error GoTo hell
    openMysqlDB ("updateNotaAssegnazione")
    mySQL = "UPDATE movimenti SET NotaAssegnazione='" & cleanSQL(myText) & "' WHERE IDMovimento = " & myIDMovimento
    myDB.Execute mySQL
    closeMysqlDB ("updateNotaAssegnazione")
    Exit Sub
hell:
    
End Sub
Private Sub deleteMovimento(myIDMovimento As Integer)
    Dim myRS As New ADODB.Recordset
    Dim mySQL As String
    On Error GoTo hell
    openMysqlDB ("deleteMovimento")
    mySQL = "DELETE FROM movimenti WHERE IDMovimento = " & myIDMovimento
    myDB.Execute mySQL
    closeMysqlDB ("deleteMovimento")
    Exit Sub
hell:
    

End Sub
Private Function getAssegnazione(myIDAssegnazione) As String
    Dim myRS As New ADODB.Recordset
    Dim mySQL As String
    On Error GoTo hell
    openMysqlDB ("getAssegnazione")
    mySQL = "SELECT * FROM assegnazioni WHERE IDAssegnazione = " & myIDAssegnazione
    myRS.CursorLocation = adUseClient
    myRS.CursorType = adOpenKeyset
    myRS.Open mySQL, myDB, adOpenForwardOnly, adLockReadOnly
    If Not myRS.EOF Then
        getAssegnazione = "" & myRS("descrizione")
    Else
        getAssegnazione = ""
    End If
    closeMysqlDB ("getAssegnazione")
    Exit Function
hell:
    getAssegnazione = ""
End Function

Private Function getTipoMateriale(myIDMateriale) As String
    Dim myRS As New ADODB.Recordset
    Dim mySQL As String
    On Error GoTo hell
    openMysqlDB ("getTipoMateriale")
    mySQL = "SELECT Tipo,IDMateriale FROM materiale WHERE IDMateriale = " & myIDMateriale
    myRS.CursorLocation = adUseClient
    myRS.CursorType = adOpenKeyset
    myRS.Open mySQL, myDB, adOpenForwardOnly, adLockReadOnly
    If Not myRS.EOF Then
        getTipoMateriale = "" & myRS("Tipo")
    Else
        getTipoMateriale = ""
    End If
    closeMysqlDB ("getTipoMateriale")
    Exit Function
hell:
    getTipoMateriale = ""
End Function

